package com.kodobeat;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;

import com.kodobeat.NanoHTTPD.Response.Status;
import com.kodobeat.cisettings.R;
import com.kodobeat.util.AndroidExec;
import com.kodobeat.util.N3ClientNetwokInfo;
import com.kodobeat.util.ReadTHINCVS;
import com.kodobeat.util.Thin;
import com.kodobeat.util.UpdateCSV;
import com.kodobeat.util.Util;

import android.R.bool;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;

public class CISettings extends NanoHTTPD {
    private final int SOCK_TIMEOUT =  6500;
	private static Context context;
	private ActivityManager am;
	private static Activity activity;
	private Properties prop;
    private String pdsmode="1";

	private static String LOG_TAG ="HttpdCiSettings";
	private static String nameOfViewerPkg = "it.mediamarket.instoretablet";
	private static String nameOfSPkg = "it.mediamarket.instoretablet";
	private static String nameOfSettingPkg = "com.kodobeat.cisettings";
	private static String nameOfClass = "com.vipera.dynamicengine.DESplashActivity";
	private static String wwwadminpath ="/admin/";
	
	//private static String fileadminpath ="/admin/";
	//private static String fileshotpath ="/admin/"; // sara /

	private static String fileadminpath ="/";
	private static String fileshotpath ="/"; // sara /

	private static String MODE_DIGITAL_SIGNAGE ="1";
	private static String MODE_QUEUE ="2";


	private static String networkpath ="/network.do";
	private static String networwifikpath ="/netwifi.do";
	private static String datapath ="/data.do";
	private static String monitorpath ="/monitor.do";
	private static String settingspath ="/settings.do";
	private static String viewerpath ="/viewer.do";
	private static String machinepath ="/machine.do";
	private static String shotpath ="/shot.do";
	private static String advancepath ="/advanced.do";
	private static String adbpath ="adbnetwork.do";
	

	private static String fileproperties = "/sdcard/config.properties";

	private AssetManager assetManager;
	private ReadTHINCVS readThincvs;
	private String nomeMonitor ="NOME_MONITOR";
	private boolean requireRestart = false;
	private int secforEachShot = 120;
	private int checkforConnectivity = 10;
	private boolean isInError = true;
	private String defaultpage ="index.html";
	// Thread per gli shpts
	class ScreenShotThread extends Thread {
		public ScreenShotThread(String localip) {
			super(localip);
		}
		public void run() {
			while(true){
				try {
					Log.d(LOG_TAG, "Shotting Screen");
					makeShotScreen();
					sleep(1000*secforEachShot);//2 minuti

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					Log.e(LOG_TAG, e.getMessage());
				}

			}
		}
	}

	public void restoreBackUrlcheck () {
		String back_ip = prop.getProperty("instoreip_backip");
		String url_adv = prop.getProperty("url");
		String ipinstore =  prop.getProperty("ip-in-store");
		
		if (url_adv.contains(back_ip)){
			String defaulturl = url_adv.replace(back_ip, ipinstore);
			prop.setProperty("url", defaulturl);
			try {
				saveProperties();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	
	
public String getAvaibleInStoreUrl(String checkurl ) {
		
		Log.d(LOG_TAG, " in getAvaibleInStoreUrl");
		String back_ip = prop.getProperty("instoreip_backip");
		String url_adv = prop.getProperty("url");
		String ipinstore =  prop.getProperty("ip-in-store");
		HttpURLConnection connection;
		boolean nomeServer = false;
		
		String primarycheckURL = "";
		String secomdarycheckURL = "";
		
		if (url_adv.contains(back_ip)){ //nome del secondario
			secomdarycheckURL = url_adv;
			primarycheckURL= url_adv.replace(back_ip, ipinstore);
			
		}else{
			primarycheckURL = url_adv;
			secomdarycheckURL = url_adv.replace(ipinstore, back_ip);
		}

		//PRIMARY CHECK
		try {
			// controllo sempre il primo
				connection = (HttpURLConnection) new URL(primarycheckURL).openConnection();
				connection.setRequestProperty("User-Agent","ThinClient");
				connection.setConnectTimeout(SOCK_TIMEOUT);
				connection.setReadTimeout(SOCK_TIMEOUT);
				connection.setRequestMethod("HEAD");
				int responseCode = connection.getResponseCode();
				Log.d(LOG_TAG, "getAvaibleInStoreUrl response "+responseCode + "for "+primarycheckURL);
			if (responseCode != 200 && !nomeServer)
			{
				Log.e(LOG_TAG, "1)Primary is in ERROR "+primarycheckURL);
				Log.d(LOG_TAG, "Switch on "+secomdarycheckURL);
				prop.setProperty("url", secomdarycheckURL);
				
				
				try {
					saveProperties();
					
				} catch (Exception e1) {
					Log.e(LOG_TAG, "InError  on saveProperties"+e1.getMessage());
					// TODO Auto-generated catch block
				}
				
				
			}else{// OK
				if (url_adv.contains(back_ip)) isInError = false;
				Log.d(LOG_TAG, "Primary is OK");
				prop.setProperty("url", primarycheckURL);
				
				try {
					saveProperties();
					
				} catch (Exception e1) {
					Log.e(LOG_TAG, "InError  on saveProperties "+e1.getMessage());
					// TODO Auto-generated catch block
				}

				return primarycheckURL;
			}
			
			
		} 
		catch (Exception e) {
			Log.e(LOG_TAG, "2)Primary is in ERROR "+e.getMessage());
				Log.d(LOG_TAG, "Switch on "+secomdarycheckURL);
				prop.setProperty("url", secomdarycheckURL);
				
				
				try {
					saveProperties();
					
				} catch (Exception e1) {
					Log.e(LOG_TAG, "InError  on saveProperties "+e.getMessage());
					// TODO Auto-generated catch block
				}

		}
		
	//SECONDARY CHECK
		try {
			// controllo sempre il primo
				connection = (HttpURLConnection) new URL(secomdarycheckURL).openConnection();
				connection.setRequestProperty("User-Agent","ThinClient");
				connection.setConnectTimeout(SOCK_TIMEOUT);
				connection.setReadTimeout(SOCK_TIMEOUT);
				connection.setRequestMethod("HEAD");
				int responseCode = connection.getResponseCode();
				Log.d(LOG_TAG, "getAvaibleInStoreUrl response "+responseCode + "for "+secomdarycheckURL);
			if (responseCode != 200 && !nomeServer && responseCode != 401)
			{
				Log.e(LOG_TAG, "secomdarycheckURL is in ERROR "+ secomdarycheckURL);
				Log.d(LOG_TAG, "Switch on "+primarycheckURL);
				prop.setProperty("url", primarycheckURL);
				
				
				try {
					saveProperties();
					
				} catch (Exception e1) {
					Log.e(LOG_TAG, "InError  on saveProperties "+e1.getMessage() );
					// TODO Auto-generated catch block
				}
				
				
			} else{//OK
				if (url_adv.contains(ipinstore)) isInError = false;
				Log.d(LOG_TAG, "Secondary is OK");
				prop.setProperty("url", secomdarycheckURL);
				
				try {
					saveProperties();
					
				} catch (Exception e1) {
					Log.e(LOG_TAG, "InError  on saveProperties "+e1.getMessage());
					// TODO Auto-generated catch block
				}

				
				return secomdarycheckURL;
			}
			
			
		} 
		catch (Exception e) {
				Log.e(LOG_TAG, "secomdarycheckURL is in ERROR "+e.getMessage());
				Log.d(LOG_TAG, "Switch on "+primarycheckURL);
				prop.setProperty("url", primarycheckURL);
				
				
				try {
					saveProperties();
					
				} catch (Exception e1) {
					Log.e(LOG_TAG, "InError  on saveProperties "+e1.getMessage());
					// TODO Auto-generated catch block
				}

		}
		
		Log.e(LOG_TAG, "Primary and Secondary are in  ERROR");
		Log.d(LOG_TAG, "Switch on "+secomdarycheckURL);
		prop.setProperty("url", secomdarycheckURL);
		return secomdarycheckURL;
		
		
		
	}


	private boolean isAppOnForeground(Context context) {
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> appProcesses = activityManager.getRunningServices(Integer.MAX_VALUE);


		if (appProcesses == null) {
			return false;
		}
		final String packageName = nameOfViewerPkg;
		for (ActivityManager.RunningServiceInfo appProcess : appProcesses) {

			if (appProcess.process.equals(packageName)) {
				return true;
			}
		}
		return false;
	}


		// Thread per la connectivity
	class CheckConnectivityThread extends Thread {
		public CheckConnectivityThread(String localip) {
			super(localip);
		}
		public void run() {
			//String primaryurl = getName();
			
			while(true){
				try {

					// controlla lo stato dsMode Gestione delle <code>1 o 2 </code>
					String dsMode = prop.getProperty("dsMode",pdsmode);
					String url = prop.getProperty("url");
					Log.d(LOG_TAG, "Connectivity on " + url);
					String checkurl ="";

					/*if (dsMode.equalsIgnoreCase(MODE_DIGITAL_SIGNAGE)) {
						checkurl = getAvaibleInStoreUrl(url);
						//url=checkurl; //facendo cosi nn torna mai sul primo
					}else
					{
						checkurl = url;
					}
					*/
						checkurl = url;


					Log.d(LOG_TAG, "isAppOnForeground  " + isAppOnForeground(context));
					if (!isAppOnForeground(context)) {
						Intent in = new Intent(Intent.ACTION_MAIN);
						in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						in.addCategory(Intent.CATEGORY_LAUNCHER);
						in.putExtra("url", "E");//E=error
						in.setClassName(nameOfViewerPkg, nameOfClass);
						context.startActivity(in);
					}
						Log.d(LOG_TAG, "checkforConnectivity on server " + checkurl);

						HttpURLConnection connection;
						try {
							Log.d(LOG_TAG, "start check ");
							connection = (HttpURLConnection) new URL(checkurl).openConnection();
							connection.setRequestProperty("User-Agent", "ThinClient");
							connection.setConnectTimeout(SOCK_TIMEOUT);
							connection.setReadTimeout(SOCK_TIMEOUT);
							connection.setRequestMethod("HEAD");
							//android bug http://stackoverflow.com/questions/17638398/androids-httpurlconnection-throws-eofexception-on-head-requests
							connection.setRequestProperty("Accept-Encoding", "");

							Log.d(LOG_TAG, "Wait for response code ");
							int responseCode = connection.getResponseCode();
							Log.d(LOG_TAG, "response code " +responseCode);
							//MARCO ERRORE fatto a mano
							//isInError =false;

							if (responseCode !=200) {
								// Not OK. Stop Viewer
								Log.d(LOG_TAG, "Not OK. isInError " + isInError);

									stopViewer();
									setWallPaper();

							}

						} catch (Exception e) {
							//Errore Genrico
							Log.e(LOG_TAG, "Error in CheckConnectivityThread");
							e.printStackTrace();
							if (isInError) {
								Log.e(LOG_TAG, "Check Error");
								stopViewer();




								isInError = false;
								//url = primaryurl;
								setWallPaper();

							}



						}



					sleep(1000*checkforConnectivity);//2 minuti


				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					Log.e(LOG_TAG, e.getMessage());
				}

				
				
			}
		}
	}

public void readDB(){
	InputStream cvs=null;
	try {
		/*Nuova logica
		 * */
		File fdb = new File("/sdcard/db.csv");
		if (fdb.exists())
		{//leggo il file da sdcard
			Log.d(LOG_TAG,"READING  /sdcard/db.csv");
			cvs = new BufferedInputStream(new FileInputStream(fdb));
		}else{
			// leggo il file dal assest
			Log.d(LOG_TAG,"READING  internal IP_THIN_CLIENT.csv");
			cvs = assetManager.open("IP_THIN_CLIENT.csv");
		}
		readThincvs  = new ReadTHINCVS(cvs);
		//thread Screenshot

	} catch (IOException e) {
		// TODO Auto-generated catch block
		Log.e(LOG_TAG, e.getMessage());
	}
}

	public CISettings(Context context,String hostname){
        super(hostname,8080);
        CISettings.context = context;
        am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        assetManager = context.getAssets();
        secforEachShot = 120; // screenshot time
        nomeMonitor ="monitor";
        new ScreenShotThread(hostname).start();
        readDB();

    }

	public CISettings(Activity activity,String hostname) {
		super(hostname,8080);
		// TODO Auto-generated constructor stub

		CISettings.activity = activity;
		CISettings.context = activity.getApplicationContext();
		

		am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		assetManager = activity.getAssets();
		prop = new Properties();
		InputStream cvs = null;
		
		readDB();

		InputStream propIn = null;
		Log.d(LOG_TAG,"fileproperties :"+fileproperties);
		try {
			propIn = new FileInputStream(fileproperties);
			prop.load(propIn);

			if (null!=prop && null!=prop.getProperty("monitor-name")){

				nomeMonitor = prop.getProperty("monitor-name");
				Log.d(LOG_TAG, "MONITOR_NAME = "+nomeMonitor);
			}
			
			if (null!=prop && null!=prop.getProperty("shot-intervall")){
				secforEachShot = Integer.parseInt(prop.getProperty("shot-intervall"));
				Log.d(LOG_TAG, "ScreenShotThread  start with param "+secforEachShot+ "sec");
			}

			
			// Running Thread only if not local
			Log.d(LOG_TAG, "autostart "+prop.getProperty("autostart"));
			if (!hostname.equalsIgnoreCase("127.0.0.1") && (null!= prop && null!=prop.getProperty("autostart") && prop.getProperty("autostart").equalsIgnoreCase("true"))){
				Log.d(LOG_TAG, "Restore Defaults URL instore Ip ");
				restoreBackUrlcheck();
				Log.d(LOG_TAG, "Start Threads");
				new ScreenShotThread(hostname).start();

				if (null!=prop && null!=prop.getProperty("netcheck-intervall")){
					checkforConnectivity = Integer.parseInt(prop.getProperty("netcheck-intervall"));
					Log.d(LOG_TAG, "CheckConnectivityThread  start with param "+checkforConnectivity+ "sec");
				}

				// TODO
				//String testurl = "http://"+prop.getProperty("ip-in-store")+":8080";
				String testurl = prop.getProperty("url");
				
				new CheckConnectivityThread(testurl).start();
			}

			


		} catch (FileNotFoundException e) {
			Log.e(LOG_TAG,e.getMessage());
			//	return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());
		} catch (Exception e) {
			Log.e(LOG_TAG,e.getMessage());
			//	return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());
		}finally {
			Util.closeInputStream(propIn);
		}



		// WALLPAPER DEFAULT	
		setWallPaper();
		
		/*int pidsettings = findPIDbyPackageName(	nameOfSettingPkg );
		int pidviewer= findPIDbyPackageName(	nameOfViewerPkg );
		Log.i(LOG_TAG, "PID Settings = "+pidsettings +" PID viewer = "+pidviewer);
		
		AndroidExec axpid = new AndroidExec();
		String[] axpidcommands = {"echo \"-17\" > /proc/"+pidsettings+"/oom_adj"};
		axpid.RunAsRoot(axpidcommands);
		*/

	}
	
	
	
	 private String intToIpAddress(int ip){
	 String ipString = String.format("%d.%d.%d.%d",(ip & 0xff),(ip >> 8 & 0xff),(ip >> 16 & 0xff),(ip >> 24 & 0xff));
	 return ipString;
	 }

    private void restartBrowser(){

        //STOP
        stopViewer();


        //START
        Intent in = new Intent(Intent.ACTION_MAIN);
        in.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
        in.addCategory(Intent.CATEGORY_LAUNCHER);
        in.putExtra("url", "#");//#=ok
        in.setClassName(nameOfViewerPkg, nameOfClass);
        context.startActivity(in);
    }


	private void  setWallPaper() {
		WallpaperManager wallpaperManager = WallpaperManager.getInstance(activity.getApplicationContext());
		// WALLPAPER DEFAULT	
				String wallPaper ="";
				InputStream bitmap=null;
				Bitmap bit = null;
				try {

					if (null!=prop && null!=prop.getProperty("brand") && null!=prop.getProperty("mode")){
				    		wallPaper = prop.getProperty("brand")+"_"+prop.getProperty("mode")+".jpg";
				    		
				    		if (!Util.assetExists(assetManager, wallPaper)) 
				    			{
				    				wallPaper = "default_"+prop.getProperty("mode")+".jpg";
				    				
				    			}
				    		Log.d(LOG_TAG,"wallPaper  try :"+wallPaper);
				    		bitmap=assetManager.open(wallPaper);
		    				bit=BitmapFactory.decodeStream(bitmap);
		    				wallpaperManager.setBitmap(bit);
		    				bitmap.close();
					}
					
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					wallPaper = "default_"+prop.getProperty("mode")+".jpg";
					Log.d(LOG_TAG,"wallPaper  try :"+wallPaper);
					Log.d(LOG_TAG, "Set Defaul WallPaper! BRAND NOT FOUND");
				    try {
					 bitmap=assetManager.open(wallPaper);
					 bit=BitmapFactory.decodeStream(bitmap);
					 wallpaperManager.setBitmap(bit);
					 bitmap.close();
				   	} 	catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					}
					
				}
	}
	
	@Override 
	public Response serve(IHTTPSession session) {
		super.serve(session);
		Method method = session.getMethod();
		String uri = session.getUri();
		Map<String, String> parms = session.getParms();
		String userAgent = session.getHeaders().get("user-agent");
		Log.d(LOG_TAG,"parms:"+parms );
		Log.d(LOG_TAG,"header:"+session.getHeaders() );
		Log.d(LOG_TAG,"user-agent:"+userAgent );
		
		if (null != userAgent && userAgent.equalsIgnoreCase("n3client")) defaultpage="default.html";
		
		/*Update csv*/
		

		if (uri.equalsIgnoreCase("/updatecsv")){
			ArrayList <Thin> brands  = readThincvs.getBrands();
			Iterator<Thin> iter = brands.iterator();

			String answer = "";
			
			if (prop.getProperty("autostart").contains("true")){
				int status = 4;
				answer="{\"status\":\""+status+"\"}";

				Response res = new NanoHTTPD.Response(answer);
				res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
				res.addHeader( "Pragma", "no-cache" );
				return res;
			}
			
			Log.d(LOG_TAG,"listAllCSV" );
			String filecsv = UpdateCSV.listAllCSV();
			Log.d(LOG_TAG,"listAllCSV result "+filecsv );

			if (filecsv.equals("ZERO")){
				int status = 1;
				answer="{\"status\":\""+status+"\"}";

				Response res = new NanoHTTPD.Response(answer);
				res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
				res.addHeader( "Pragma", "no-cache" );
				return res;
			}
			if (filecsv.equals("NOMOUNT")){
				int status = 9;
				answer="{\"status\":\""+status+"\"}";

				Response res = new NanoHTTPD.Response(answer);
				res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
				res.addHeader( "Pragma", "no-cache" );
				return res;
			}
			

			if (filecsv.equals("MANY")){
				int status = 2;
				answer="{\"status\":\""+status+"\"}";

				Response res = new NanoHTTPD.Response(answer);
				res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
				res.addHeader( "Pragma", "no-cache" );
				return res;
			}
			
			
			if (!UpdateCSV.checkHeadercsv("/usbdisk/"+filecsv)){
				int status = 3;
				answer="{\"status\":\""+status+"\"}";

				Response res = new NanoHTTPD.Response(answer);
				res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
				res.addHeader( "Pragma", "no-cache" );
				return res;
			}
			
			
			UpdateCSV.update("/usbdisk/"+filecsv);
			int status = 0;
			answer="{\"status\":\""+status+"\"}";

			Response res = new NanoHTTPD.Response(answer);
			res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
			res.addHeader( "Pragma", "no-cache" );
			return res;

			
		}


		/*Gestione Html di amministrazione*/

		if (uri.equalsIgnoreCase("/getbrands")){
			ArrayList <Thin> brands  = readThincvs.getBrands();
			Iterator<Thin> iter = brands.iterator();

			String answer = "[";
			while (iter.hasNext()) {
				Thin thin = iter.next();
				if (iter.hasNext())
					answer+="{\"name\":\""+thin.getBrand()+"\"},";
				else
					answer+="{\"name\":\""+thin.getBrand()+"\"}";
			}

			answer+="] ";
			Response res = new NanoHTTPD.Response(answer);
			res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
			res.addHeader( "Pragma", "no-cache" );
			return res;
		}
		
		
		
		if (uri.equalsIgnoreCase("/getcitybybrand")){
			String brandid="";
			if (null!= parms.get("brand")){

				brandid =  parms.get("brand");
			}

			ArrayList <Thin> cities  = readThincvs.getCityByBrand(brandid);
			Iterator<Thin> iter = cities.iterator();

			String answer = "[";
			while (iter.hasNext()) {
				Thin thin = iter.next();
				if (iter.hasNext())
					answer+="{\"citta\":\""+thin.getCitta()+"\",\"id\":\""+thin.getIndex()+"\",\"tot\":\""+thin.getHowmany()+"\"},";
							
					//answer+="{\"citta\":\""+thin.getCitta()+"\",\"id\":\""+thin.getIndex()+"\",\"tot\":\""+thin.getHowmany()+"\",\"monitornames\":["+thin.getMonitor_name()+"]},";
				else
					//answer+="{\"citta\":\""+thin.getCitta()+"\",\"id\":\""+thin.getIndex()+"\",\"tot\":\""+thin.getHowmany()+"\",\"monitornames\":["+thin.getMonitor_name()+"]}";
					answer+="{\"citta\":\""+thin.getCitta()+"\",\"id\":\""+thin.getIndex()+"\",\"tot\":\""+thin.getHowmany()+"\"}";
					
			}

			answer+="] ";

			Response res = new NanoHTTPD.Response(answer);
			res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
			res.addHeader( "Pragma", "no-cache" );

			return res;
		}

		
		if (uri.equalsIgnoreCase("/getmonitornamebyid")){
			int id = 0;
			if (null!= parms.get("id")){
				id =  Integer.parseInt(parms.get("id"));
			}
			String answer ="";
			Thin tot = readThincvs.gettotByID(id);
			if (tot!=null)
				
				answer = "{\"monitor\":"+tot.getMonitor_name()+"}";
				//answer = readThincvs.getMonitorNameByID(id);
			//answer = "{\"monitor\":"+tot.getMonitor_name()+"}";
			
			else
				answer = "{\"monitor\":\"-\"}";

			Response res = new NanoHTTPD.Response(answer);
			res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
			res.addHeader( "Pragma", "no-cache" );
			return res;
		}
		

		if (uri.equalsIgnoreCase("/gettotbyid")){
			int id = 0;
			if (null!= parms.get("id")){
				id =  Integer.parseInt(parms.get("id"));
			}
			String answer ="";
			Thin tot = readThincvs.gettotByID(id);
			if (tot!=null)
				//answer = "{\"tot\":"+tot.getHowmany()+"}";
					answer = "{\"tot\":"+tot.getHowmany()+",\"monitornames\":["+readThincvs.getMonitorNameByID(id)+"]}";
				
			else
				answer = "{\"tot\":\"0\"}";

			Response res = new NanoHTTPD.Response(answer);
			res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
			res.addHeader( "Pragma", "no-cache" );
			return res;
		}


		if (uri.equalsIgnoreCase("/getnetworkbyid")){
			int id = 0;
			if (null!= parms.get("id")){

				id = Integer.parseInt(parms.get("id"));
			}
			ArrayList <Thin> networkinfo  = readThincvs.getNetworkbyID(id);
			Iterator<Thin> iter = networkinfo.iterator();

			String answer = "";
			while (iter.hasNext()) {
				Thin thin = iter.next();
				answer+="{\"brand\":\""+thin.getBrand()+"\",\"ip\":\""+thin.getIp()+"\",\"netmask\":\""+thin.getSottorete()+"\",\"gateway\":\""+thin.getGateway()+"\","
						+"\"storecode\":\""+thin.getCodice()+"\","
						+ "\"dns1\":\""+thin.getDns1()+"\",\"dns2\":\""+thin.getDns2()+"\",\"hostname\":\""+thin.getHostname()+"\",\"instoreip\":\""+thin.getInstoreip()+"\",\"instoreip_backip\":\""+thin.getInstoreip_backup()+"\"}";
			}	

			Response res = new NanoHTTPD.Response(answer);
			res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
			res.addHeader( "Pragma", "no-cache" );
			return res;
		}




		if (uri.equalsIgnoreCase("/getprop")){
			
			String settingversion ="0";
			String viewerversion ="0";
			
			N3ClientNetwokInfo info = new N3ClientNetwokInfo();
			
			String interfacename = prop.getProperty("netinterface", "eth0");
			String ipDetect =  info.getIPbydev(interfacename);
			
			
			
			try {
				PackageInfo pInfosettings = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
				settingversion = pInfosettings.versionCode+"."+pInfosettings.versionName;
				
				PackageInfo pInfoviewer = activity.getPackageManager().getPackageInfo(nameOfViewerPkg, 0);
				viewerversion = pInfoviewer.versionCode+"."+pInfoviewer.versionName;
				
				  
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				settingversion = "0";
			}
			
			String dbcsv="internal";
			
			File filedb = new File("/sdcard/db.csv");
			if (filedb.exists()){
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				dbcsv ="USB - DATE: "+ sdf.format(filedb.lastModified());
			}

			String answer ="";
			
			if (interfacename.equalsIgnoreCase("wlan0")){
				WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
				DhcpInfo d = wifiManager.getDhcpInfo();
				ipDetect = intToIpAddress(d.ipAddress);
				if (prop.getProperty("ipstatic").equalsIgnoreCase("true")) {
				 answer = "{\"url\":\""+prop.getProperty("url")+"\","+
							"\"audio\":\""+prop.getProperty("audio")+"\","+
							"\"dbCSV\":\""+dbcsv+"\","+
							"\"version-settings\":\""+settingversion+"\","+
							"\"autostart\":\""+prop.getProperty("autostart")+"\","+
							"\"netmask\":\""+ prop.getProperty("netmask","dhcpmode")+"\","+
							"\"ip\":\""+ipDetect+"\","+
							"\"updsettingsver\":\""+prop.getProperty("settingversion","0")+"\","+
							"\"updviewerver\":\""+prop.getProperty("viewerversion","0")+"\","+
							"\"activeUrl\":\""+"http://"+prop.getProperty("activeUrl","")+prop.getProperty("baseurl")+"\","+
							"\"gateway\":\""+intToIpAddress(d.gateway)+"\","+
							"\"dns1\":\""+intToIpAddress(d.dns1)+"\","+
							"\"dns2\":\""+intToIpAddress(d.dns2)+"\","+
							"\"ipstatic\":\""+prop.getProperty("ipstatic")+"\","+
							"\"version-viewer\":\""+viewerversion+"\","+
							"\"storecode\":\""+prop.getProperty("storecode")+"\","+
							"\"baseurl\":\""+prop.getProperty("baseurl")+"\","+
							"\"brand\":\""+prop.getProperty("brand")+"\","+	
							"\"ip-in-store\":\""+prop.getProperty("ip-in-store")+"\","+	
							"\"instoreip_backip\":\""+prop.getProperty("instoreip_backip")+"\","+	
							"\"hostname\":\""+prop.getProperty("hostname")+"\","+
							"\"urljsoncontent\":\""+prop.getProperty("urljsoncontent")+"\","+	
							"\"monitor-name\":\""+prop.getProperty("monitor-name")+"\","+
							"\"netcheck-intervall\":\""+prop.getProperty("netcheck-intervall")+"\","+
							"\"cache-browser\":\""+prop.getProperty("cache-browser")+"\","+
							"\"shot-intervall\":\""+prop.getProperty("shot-intervall")+"\","+
							"\"netinterface\":\""+prop.getProperty("netinterface","eth0")+"\","+
							"\"wifi-ssid\":\""+prop.getProperty("SSID-name","")+"\","+
							"\"wifi-password\":\""+prop.getProperty("SSID-secret","")+"\","+
							"\"netinterface\":\""+prop.getProperty("netinterface","eth0")+"\","+
							"\"mode\":\""+prop.getProperty("rotation")+"\"}";
				 
				}else{
					//DHCP
					 answer = "{\"url\":\""+prop.getProperty("url")+"\","+
								"\"audio\":\""+prop.getProperty("audio")+"\","+
								"\"version-settings\":\""+settingversion+"\","+
								"\"autostart\":\""+prop.getProperty("autostart")+"\","+
								"\"dbCSV\":\""+dbcsv+"\","+
								"\"netmask\":\""+ intToIpAddress(d.netmask)+"\","+
								"\"ip\":\""+ipDetect+"\","+
								"\"updsettingsver\":\""+prop.getProperty("settingversion","0")+"\","+
								"\"updviewerver\":\""+prop.getProperty("viewerversion","0")+"\","+
								"\"activeUrl\":\""+"http://"+prop.getProperty("activeUrl","")+prop.getProperty("baseurl")+"\","+
								"\"gateway\":\""+intToIpAddress(d.gateway)+"\","+
								"\"dns1\":\""+intToIpAddress(d.dns1)+"\","+
								"\"dns2\":\""+intToIpAddress(d.dns2)+"\","+
								"\"ipstatic\":\""+prop.getProperty("ipstatic")+"\","+
								"\"version-viewer\":\""+viewerversion+"\","+
								"\"storecode\":\""+prop.getProperty("storecode")+"\","+
								"\"baseurl\":\""+prop.getProperty("baseurl")+"\","+
								"\"brand\":\""+prop.getProperty("brand")+"\","+	
								"\"ip-in-store\":\""+prop.getProperty("ip-in-store")+"\","+	
								"\"instoreip_backip\":\""+prop.getProperty("instoreip_backip")+"\","+	
								"\"hostname\":\""+prop.getProperty("hostname")+"\","+
								"\"urljsoncontent\":\""+prop.getProperty("urljsoncontent")+"\","+	
								"\"monitor-name\":\""+prop.getProperty("monitor-name")+"\","+
								"\"netcheck-intervall\":\""+prop.getProperty("netcheck-intervall")+"\","+
								"\"cache-browser\":\""+prop.getProperty("cache-browser")+"\","+
								"\"shot-intervall\":\""+prop.getProperty("shot-intervall")+"\","+
								"\"netinterface\":\""+prop.getProperty("netinterface","eth0")+"\","+
								"\"wifi-ssid\":\""+prop.getProperty("SSID-name","")+"\","+
								"\"wifi-password\":\""+prop.getProperty("SSID-secret","")+"\","+
								"\"netinterface\":\""+prop.getProperty("netinterface","eth0")+"\","+
								"\"mode\":\""+prop.getProperty("rotation")+"\"}";
				}

			}else{
			
			

			 answer = "{\"url\":\""+prop.getProperty("url")+"\","+
					"\"audio\":\""+prop.getProperty("audio")+"\","+
					"\"version-settings\":\""+settingversion+"\","+
					"\"dbCSV\":\""+dbcsv+"\","+
					"\"autostart\":\""+prop.getProperty("autostart")+"\","+
					"\"netmask\":\""+prop.getProperty("netmask","dhcpmode")+"\","+
					"\"ip\":\""+ipDetect+"\","+
					"\"updsettingsver\":\""+prop.getProperty("settingversion","0")+"\","+
					"\"updviewerver\":\""+prop.getProperty("viewerversion","0")+"\","+
					"\"activeUrl\":\""+"http://"+prop.getProperty("activeUrl","")+prop.getProperty("baseurl")+"\","+
					"\"gateway\":\""+prop.getProperty("gateway","dhcpmode")+"\","+
					"\"dns1\":\""+prop.getProperty("dns1","dhcpmode")+"\","+
					"\"dns2\":\""+prop.getProperty("dns2","dhcpmode")+"\","+
					"\"ipstatic\":\""+prop.getProperty("ipstatic")+"\","+
					"\"version-viewer\":\""+viewerversion+"\","+
					"\"storecode\":\""+prop.getProperty("storecode")+"\","+
					"\"baseurl\":\""+prop.getProperty("baseurl")+"\","+
					"\"brand\":\""+prop.getProperty("brand")+"\","+	
					"\"ip-in-store\":\""+prop.getProperty("ip-in-store")+"\","+	
					"\"instoreip_backip\":\""+prop.getProperty("instoreip_backip")+"\","+	
					"\"hostname\":\""+prop.getProperty("hostname")+"\","+
					"\"urljsoncontent\":\""+prop.getProperty("urljsoncontent")+"\","+	
					"\"monitor-name\":\""+prop.getProperty("monitor-name")+"\","+
					"\"netcheck-intervall\":\""+prop.getProperty("netcheck-intervall")+"\","+
					"\"cache-browser\":\""+prop.getProperty("cache-browser")+"\","+
					"\"shot-intervall\":\""+prop.getProperty("shot-intervall")+"\","+
					"\"netinterface\":\""+prop.getProperty("netinterface","eth0")+"\","+
					"\"wifi-ssid\":\""+prop.getProperty("SSID-name","")+"\","+
					"\"wifi-password\":\""+prop.getProperty("SSID-secret","")+"\","+
					"\"netinterface\":\""+prop.getProperty("netinterface","eth0")+"\","+
					"\"mode\":\""+prop.getProperty("rotation")+"\"}";

			}
			 
			Response res = new NanoHTTPD.Response(answer);
			res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
			res.addHeader( "Pragma", "no-cache" );
			return res;
		}


		if (uri.contains(adbpath)){
			
			AndroidExec ax = new AndroidExec();
			String[] commands = {"setprop service.adb.tcp.port 5555","stop adbd","start adbd"};
			ax.RunAsRoot(commands);

			return new NanoHTTPD.Response("Success");
		}
		
		if (uri.contains(advancepath)){
			Log.d(LOG_TAG,"advancepath"+parms);
			String cache="";
			String shotint="";
			String netint="";
		
			
				if  (null != parms.get("cache-browser") ){
					cache=parms.get("cache-browser");
					prop.setProperty("cache-browser", cache);
				}
				if  (null != parms.get("shot-intervall") ){
					shotint=parms.get("shot-intervall");
					prop.setProperty("shot-intervall", shotint);
				}

				if  (null != parms.get("netcheck-intervall") ){
					netint=parms.get("netcheck-intervall");
					prop.setProperty("netcheck-intervall", netint);
				}
				
				try {
					requireRestart = true;
					savePropertiesWithBackup();

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());
				}
				return new  NanoHTTPD.Response(Status.OK,MIME_HTML,"Success");

		}


		
		if (uri.equalsIgnoreCase("/override.do")){
			String overurl = "";
			if  (null != parms.get("manualurl") ){
				overurl=parms.get("manualurl");
			}
			Log.d(LOG_TAG,"manualurl:"+overurl );
			
			if (!overurl.startsWith("http"))
			{
				Log.d(LOG_TAG,"relative URL" );
				
				String newurl ="http://"+ prop.getProperty("ip-in-store")+overurl;
				prop.setProperty("url",newurl);
			
			}else{
				Log.d(LOG_TAG,"Absolute URL" );
				
				String newurl=overurl;
				prop.setProperty("url",newurl);
				
			}
			prop.setProperty("autostart", "true");
			prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
			requireRestart = true;
			try {
				savePropertiesWithBackup();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());
			}
			
			//return new NanoHTTPD.Response("Success");
		}


		
		
		
		
		
		if (uri.equalsIgnoreCase("/getstatus")){
			String answer =  "{\"viewer\":stopped}";


			Response res = new NanoHTTPD.Response(answer);
			res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
			res.addHeader( "Pragma", "no-cache" );

			return res;
		}


		if (!isExternalStorageReadable()){
			return new  NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,"isExternalStorageReadable =false ");
		}

		/*
		 * Gestione Html di amministrazione
		 *
		 */




		if (uri.contains(wwwadminpath)  ){
			// FROM ASSEST 
			InputStream fis = null;
			
			// FROM SDCARD
			//FileInputStream fis = null;
			
			Response res = null;
			try {
				// Opening file from SD Card
				String filename = FilenameUtils.getName(uri);
				Log.w(LOG_TAG, "filename by get :"+filename);
				// FROM ASSEST 
				fis = assetManager.open(filename);
				
				// FROM SDCARD
				//File root = Environment.getExternalStorageDirectory();
				//fis = new FileInputStream(root.getAbsolutePath() +fileadminpath+filename);

				if (filename.contains(".png")){
					res = new NanoHTTPD.Response(Status.OK,MIME_PNG,fis);
				}
				if (filename.contains(".gif")){
					res = new NanoHTTPD.Response(Status.OK,MIME_GIF,fis);
				}

				if (filename.contains(".css"))
					res  =  new NanoHTTPD.Response(Status.OK,MIME_CSS,fis);
				if (filename.contains(".js"))
					res =  new NanoHTTPD.Response(Status.OK,MIME_JS,fis);
				if (filename.contains(".html"))
					res =  new NanoHTTPD.Response(Status.OK,MIME_HTML,fis);

				if (filename.contains(".ttf"))
					res =  new NanoHTTPD.Response(Status.OK,MIME_TTF,fis);
				
				return res;
			}
			catch(IOException ioe) {
				Log.e(LOG_TAG," serve files"+ ioe.toString());
				return new NanoHTTPD.Response(Status.NOT_FOUND,MIME_HTML,"");
			}finally {
				//closeInputStream(fis);
			}

		}

		if (uri.equalsIgnoreCase("/getjsonurl")){
			
			String jsonurl ="";
			if(null != parms.get("url")){
				jsonurl =  parms.get("url");
			
				HttpURLConnection connection;
				try {
					connection = (HttpURLConnection) new URL(jsonurl).openConnection();
					connection.setRequestProperty("User-Agent","ThinClient");
					connection.setConnectTimeout(2000);
					connection.setReadTimeout(2000);
					connection.setRequestMethod("GET");
					int responseCode = connection.getResponseCode();
					Log.d(LOG_TAG, "getjsonurl response "+responseCode);
					if (responseCode == 200) {
						// Not OK. Stop Viewer
					}else{
						return new NanoHTTPD.Response(Status.NOT_FOUND,MIME_HTML,"");
					}
					}catch (Exception e)
					{
						return new NanoHTTPD.Response(Status.INTERNAL_ERROR ,MIME_HTML,"");
						
					}
				
				try {
					
					Response res = new NanoHTTPD.Response(Status.OK,MIME_HTML,connection.getInputStream());
					res.addHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
					res.addHeader( "Pragma", "no-cache" );
					return res;
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return new NanoHTTPD.Response(Status.INTERNAL_ERROR ,MIME_HTML,"");
				}
			}
			else
				return new NanoHTTPD.Response(Status.NOT_FOUND,MIME_HTML,"");
			
			
		}
		
		if (uri.contains("/contents/") ){
			// FROM ASSEST InputStream fis = null;
			FileInputStream fis = null;
			Response res = null;
			try {
				// Opening file from SD Card
				String filename = FilenameUtils.getName(uri);
				Log.w(LOG_TAG, "filename by get :"+filename);
				File root = Environment.getExternalStorageDirectory();
				fis = new FileInputStream(root.getAbsolutePath() +fileadminpath+filename);

				if (filename.contains(".png")){
					res = new NanoHTTPD.Response(Status.OK,MIME_PNG,fis);
				}

				return res;
			}
			catch(IOException ioe) {
				Log.e(LOG_TAG," serve files"+ ioe.toString());
				return new NanoHTTPD.Response(Status.NOT_FOUND,MIME_HTML,"");
			}finally {
				//closeInputStream(fis);
			}

		}

/* Gestione wifi 
 * 
 */
		
		if (uri.equalsIgnoreCase(networwifikpath)){

			Log.d(LOG_TAG,"networkWIFIpath"+parms);
			String ip="";
			String netmask="";
			String gateway="";
			String dns1="";
			String dns2="";
			String instoreip="";
			String nomemacchina="";
			String hostname="";
			String brand="";
			String instoreip_backip ="";
			String baseurl ="/";
			String storecode ="000";
			String netinterface ="wlan0";
			String ssid ="??";
			String ssidsecret ="secret";
			
			if  (null != parms.get("SSID") ){
				ssid=parms.get("SSID");
			}
			if  (null != parms.get("wpapass") ){
				ssidsecret=parms.get("wpapass");
			}
			if  (null != parms.get("baseurl") ){
				baseurl=parms.get("baseurl");
			}
			if  (null != parms.get("storecode") ){
				storecode=parms.get("storecode");
			}
			if  (null != parms.get("brand") ){
				brand=parms.get("brand");
				prop.setProperty("brand", brand);
			}
			if  (null != parms.get("instoreip_backip")  )
			{
				Log.d(LOG_TAG,"setting instoreip_backip");
				prop.setProperty("instoreip_backip", parms.get("instoreip_backip"));
			}
			
			
			

			if  (null != parms.get("instoreip") ){
				instoreip=parms.get("instoreip");
				prop.setProperty("ip-in-store", instoreip);
				prop.setProperty("activeUrl", instoreip);// imposto active url alla prima conf
			}
			if  (null != parms.get("nomemacchina") ){
				nomemacchina=parms.get("nomemacchina");
				prop.setProperty("monitor-name", nomemacchina);
			}

			if  (null != parms.get("urljsoncontent")  )
			{
				Log.d(LOG_TAG,"setting urljsoncontent");
				prop.setProperty("urljsoncontent", parms.get("urljsoncontent"));
			}
			
			
			//IP STATICO
			if (null!= parms.get("dhcpmode")  && parms.get("dhcpmode").equalsIgnoreCase("false")){

				
				if  (null != parms.get("hostname") ){
					hostname=parms.get("hostname");
					prop.setProperty("hostname", hostname);
				}


				if  (null != parms.get("ip") ){
					ip=parms.get("ip");
				}
				if  (null != parms.get("netmask") ){
					netmask=parms.get("netmask");
				}
				if  (null != parms.get("gateway") ){
					gateway=parms.get("gateway");
				}
				if  (null != parms.get("dns1") ){
					dns1=parms.get("dns1");
				}
				if  (null != parms.get("dns2") ){
					dns2=parms.get("dns2");
				}
				
			
			
				Log.d(LOG_TAG,"setting network");
				prop.setProperty("ip", ip);
				prop.setProperty("netmask", netmask);
				prop.setProperty("gateway", gateway);
				prop.setProperty("dns1", dns1);
				prop.setProperty("dns2", dns2);
				
				prop.setProperty("ipstatic", "true");
				prop.setProperty("storecode", storecode);
				prop.setProperty("baseurl", baseurl);
				
				if (null != userAgent && userAgent.equalsIgnoreCase("n3client")) {
					prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#tabs-1");
				}else
				{
					prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
				}
				
				prop.setProperty("netinterface",netinterface);
				prop.setProperty("SSID-name", ssid);
				prop.setProperty("SSID-secret", ssidsecret);

			}
			//IP DINAMICO
			else if (null!= parms.get("dhcpmode")  && parms.get("dhcpmode").equalsIgnoreCase("true"))
			{
				
				prop.setProperty("ipstatic", "false");
				Log.d(LOG_TAG,"setting network");
				if (null != userAgent && userAgent.equalsIgnoreCase("n3client")) {
					prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#tabs-1");
				}else
				{
					prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
				}
				if  (null != parms.get("hostname") ){
					hostname=parms.get("hostname");
					prop.setProperty("hostname", hostname);
				}
				if  (null != parms.get("brand") ){
					brand=parms.get("brand");
					prop.setProperty("brand", brand);
				}
				prop.setProperty("storecode", storecode);
				prop.setProperty("netinterface",netinterface);
				prop.setProperty("SSID-name", ssid);
				prop.setProperty("SSID-secret", ssidsecret);
			}

			requireRestart = true;
			
			try {
				savePropertiesWithBackup();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());
			}
			
			Response res  = new NanoHTTPD.Response(Status.REDIRECT,MIME_HTML,"");
			res.addHeader( "Location", "/admin/"+defaultpage+"?#network" );
			return res;

			
			
		}


		/*
		 * Gestione Html di Networking
		 * 
		 * dhcpmode =FALSE metto nel file di prop ipstatic=true/false
		 *
		 */		
		if (uri.contains(networkpath)){
			Log.d(LOG_TAG,"networkpath"+parms);
			String ip="";
			String netmask="";
			String gateway="";
			String dns1="";
			String dns2="";
			String instoreip="";
			String nomemacchina="";
			String hostname="";
			String brand="";
			String instoreip_backip ="";
			String baseurl ="/";
			String storecode ="000";
			String netinterface ="eth0";
			
			if  (null != parms.get("baseurl") ){
				baseurl=parms.get("baseurl");
			}
			if  (null != parms.get("storecode") ){
				storecode=parms.get("storecode");
			}
			if  (null != parms.get("brand") ){
				brand=parms.get("brand");
				prop.setProperty("brand", brand);
			}
			if  (null != parms.get("instoreip_backip")  )
			{
				Log.d(LOG_TAG,"setting instoreip_backip");
				prop.setProperty("instoreip_backip", parms.get("instoreip_backip"));
			}
			

			if  (null != parms.get("instoreip") ){
				instoreip=parms.get("instoreip");
				prop.setProperty("ip-in-store", instoreip);
				prop.setProperty("activeUrl", instoreip);// imposto active url alla prima conf
			}
			if  (null != parms.get("nomemacchina") ){
				nomemacchina=parms.get("nomemacchina");
				prop.setProperty("monitor-name", nomemacchina);
			}

			if  (null != parms.get("urljsoncontent")  )
			{
				Log.d(LOG_TAG,"setting urljsoncontent");
				prop.setProperty("urljsoncontent", parms.get("urljsoncontent"));
			}
			
			
			//IP STATICO
			if (null!= parms.get("dhcpmode")  && parms.get("dhcpmode").equalsIgnoreCase("false")){

				
				if  (null != parms.get("hostname") ){
					hostname=parms.get("hostname");
					prop.setProperty("hostname", hostname);
				}


				if  (null != parms.get("ip") ){
					ip=parms.get("ip");
				}
				if  (null != parms.get("netmask") ){
					netmask=parms.get("netmask");
				}
				if  (null != parms.get("gateway") ){
					gateway=parms.get("gateway");
				}
				if  (null != parms.get("dns1") ){
					dns1=parms.get("dns1");
				}
				if  (null != parms.get("dns2") ){
					dns2=parms.get("dns2");
				}
				
			
			
				Log.d(LOG_TAG,"setting network");
				prop.setProperty("ip", ip);
				prop.setProperty("netmask", netmask);
				prop.setProperty("gateway", gateway);
				prop.setProperty("dns1", dns1);
				prop.setProperty("dns2", dns2);
				
				prop.setProperty("ipstatic", "true");
				prop.setProperty("storecode", storecode);
				prop.setProperty("baseurl", baseurl);


                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

                    if (null != userAgent && userAgent.equalsIgnoreCase("n3client")) {
                        prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#tabs-1");
                    } else {
                        prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
                    }
                    prop.setProperty("netinterface", netinterface);
                } else {

                    	/* Creo il file di rete per la 6309 Inforce*

				/system/bin/dhcpcd -k eth0
				sleep 10
				ifconfig eth0 172.16.10.190 netmask 255.255.255.0
				route add default gw 172.16.10.1 dev eth0
				ndc resolver setnetdns eth0 85.18.200.200 8.8.4.4
				ndc resolver flushnet eth0
				ndc resolver clearnetdns eth0
				setprop net.eth0.ip 172.16.10.190
				setprop net.eth0.mask 255.255.255.0
				setprop net.eth0.gw 172.16.10.1
				setprop net.eth0.dns1 85.18.200.200
				setprop net.eth0.dns2 8.8.4.4
				 */

                    Log.d(LOG_TAG,"Network for Android 6.1 ");
                    String[] commands = {

                            "mount -o remount,rw /system",
                            "rm  /persist/eth0.sh",
                            "chmod 777 /persist/eth0.sh",
                            "echo /system/bin/dhcpcd -k eth0 >>/persist/eth0.sh",
                            "echo sleep 10 >>/persist/eth0.sh",
                            "echo ifconfig "+netinterface+" "+ ip+" netmask "+netmask+" >>/persist/eth0.sh",
                            "echo route add default gw "+gateway+" dev "+netinterface+" >>/persist/eth0.sh",
                            "echo ndc resolver setnetdns "+netinterface+" "+dns1 +" "+dns2 +" >>/persist/eth0.sh",
                            "echo ndc resolver flushnet "+netinterface+" >>/persist/eth0.sh",
                            "echo ndc resolver clearnetdns "+netinterface+" >>/persist/eth0.sh",
                            "echo setprop net.eth0.ip "+ip+ " >>/persist/eth0.sh",
                            "echo setprop net.eth0.mask "+netmask+ " >>/persist/eth0.sh",
                            "echo setprop net.eth0.gw "+gateway+ " >>/persist/eth0.sh",
                            "echo setprop net.eth0.dns1 "+dns1+ " >>/persist/eth0.sh",
                            "echo setprop net.eth0.dns2 "+dns2+ " >>/persist/eth0.sh",


                    };
                    AndroidExec axc_net = new AndroidExec();
                    axc_net.RunAsRoot(commands);
                    prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
                    prop.setProperty("netinterface", netinterface);
                }


			}
			//IP DINAMICO
			else if (null!= parms.get("dhcpmode")  && parms.get("dhcpmode").equalsIgnoreCase("true"))
			{
				
				prop.setProperty("ipstatic", "false");
				Log.d(LOG_TAG,"setting network");
				
				
				if (null != userAgent && userAgent.equalsIgnoreCase("n3client")) {
					prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#tabs-1");
				}else
				{
					prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
				}
			}

            prop.setProperty("autostart", "true");
			requireRestart = true;
			
			try {
				savePropertiesWithBackup();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());
			}
			
			Response res  = new NanoHTTPD.Response(Status.REDIRECT,MIME_HTML,"");
			res.addHeader( "Location", "/admin/"+defaultpage+"?#network" );
			return res;

		}

		/*
		 * Gestione Html di /data.do
		 *
		 */


		String action = parms.get("action");

		if (null!= action &&  action.equalsIgnoreCase("start") ||
				(uri.equalsIgnoreCase("/N3/device/executeScript") || uri.equalsIgnoreCase("/N3/remoteControl/executeScript") )
				&& (null!=parms.get("script") && parms.get("script").equalsIgnoreCase("startBrowser") )
				
				){
			
			Intent in = new Intent(Intent.ACTION_MAIN);
			in.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
			in.addCategory(Intent.CATEGORY_LAUNCHER);
			in.putExtra("url", "#");
			in.setClassName(nameOfViewerPkg, nameOfClass); 
			context.startActivity(in);  
			setWallPaper();
			
			return new NanoHTTPD.Response(Status.OK,MIME_HTML,"");
		}





		if (null!= action && action.equalsIgnoreCase("save")  ){
			try {
				savePropertiesWithBackup();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e(LOG_TAG,"Failed to saveProperties");
				return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());
			}
			return new NanoHTTPD.Response(Status.OK,MIME_HTML,"");
		}

		if (null!= action && action.equalsIgnoreCase("stop") ||
				(uri.equalsIgnoreCase("/N3/device/executeScript") || uri.equalsIgnoreCase("/N3/remoteControl/executeScript") )
				&& (null!=parms.get("script") && parms.get("script").equalsIgnoreCase("stopBrowser") )
				){

			stopViewer();
			//reimposta il Descktop
			//AndroidExec ax = new AndroidExec();
			//String[] commands = {"am startservice -n com.android.systemui/.SystemUIService"};
			//ax.RunAsRoot(commands);
			
			return new NanoHTTPD.Response(Status.OK,MIME_HTML,"");
		}

		if (uri.equalsIgnoreCase("/N3/device/status") || uri.equalsIgnoreCase("/N3/remoteControl/status") ){
			return new NanoHTTPD.Response(Status.OK,MIME_HTML,getPackages());
		}

		// cambia il modo dsMode da 1 a 2
		// tramite il parametreo mode=1 o mode=2
		// dsMode==1 Siamo in DigitalSignage
		// dsMode==2 Siamo in Queue
		if (uri.equalsIgnoreCase("/N3/dsMode")) {
            String actualmode = prop.getProperty("dsMode", pdsmode);
			Log.d(LOG_TAG,"Runtime dsMode "+actualmode);

			// se Eravamo in Queue e imposto DS
            if (actualmode.equalsIgnoreCase(MODE_QUEUE)) {
				Log.d(LOG_TAG, "Enabling DSmode  Moving from dsMode==2 to dsMode==1");
				String dsUrl = prop.getProperty("defaulturl");
                prop.setProperty("url",dsUrl);// imposto quello nuovo
				pdsmode = MODE_DIGITAL_SIGNAGE; //DigitalSignage Mode
                prop.setProperty("dsMode", pdsmode);
				// reset Dsurl to
				Log.d(LOG_TAG,"reset Dsurl to "+dsUrl);

                try {
                    requireRestart = false;
                    savePropertiesWithBackup();
					restartBrowser();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    return new NanoHTTPD.Response(Status.INTERNAL_ERROR, MIME_HTML, e.getMessage());
                }

                return new NanoHTTPD.Response(Status.OK, MIME_HTML, "2");
            }
            else{
				Log.d(LOG_TAG,"Enabling dsMode  Moving from dsMode==1 to dsMode==1");
				Log.d(LOG_TAG,"Action NONE");
                return new NanoHTTPD.Response(Status.OK, MIME_HTML, "1");
            }
        }
		// Aggiornamento dsMode=2
		if (uri.equalsIgnoreCase("/N3/updateUrl")){

			String newurl = parms.get("url");
			String actualmode = prop.getProperty("dsMode", pdsmode);
			Log.d(LOG_TAG,"Runtime dsMode "+actualmode);
			if (null!=newurl)
			{

				String actualUrl = prop.getProperty("url");

				//se provenivo da DigitalSignage e Passo a Queue
                if (actualmode.equalsIgnoreCase(MODE_DIGITAL_SIGNAGE)) {
					// forzo il modo a 2 = DS
					pdsmode = MODE_QUEUE;//dsMode==Queue
                    prop.setProperty("dsMode", pdsmode);
					//salvo url del Digital Signage

                    Log.d(LOG_TAG, "Saving previusUrl for DS: " + actualUrl);
					// lo salvo su una nuova variabile nel config.properties

					//check per evitare di perdere il dsURL
					if (!newurl.equalsIgnoreCase(actualUrl)) {
						// lo salvo su una nuova variabile nel config.properties

						prop.setProperty("defaulturl", actualUrl);//salvo il vecchio url del DS
						prop.setProperty("url", newurl);// imposto quello nuovo

						Log.d(LOG_TAG, "Saving Url for Queue: " + newurl);
					}

					try {
						// Salvo le propertiese per un eventualre Riavvio del Browser
						requireRestart = false;
						savePropertiesWithBackup();
						//Ogni volta che passo da DigitalSignage a Queue Restarto
						Log.d(LOG_TAG,"Restarting Viewer moving from dsMode=1 to DsMode=2 "+newurl);
						restartBrowser();


					} catch (IOException e) {
						// TODO Auto-generated catch block
						return new NanoHTTPD.Response(Status.INTERNAL_ERROR, MIME_HTML, e.getMessage());
					}
                }
				else //actualMode == 2
                {  // altrimenti se sto riscrivendo URL
					// mi assicuro di forzar URL in Queue dsMode==2
					Log.d(LOG_TAG,"Moving from dsMode==2 to DsMode==2 Url "+newurl);
					pdsmode = MODE_QUEUE;
					prop.setProperty("dsMode", pdsmode);
					prop.setProperty("url", newurl);// imposto Url quello nuovo

					try {
						// Salvo le propertiese per un eventualre Riavvio
						savePropertiesWithBackup();

					} catch (IOException e) {
						// TODO Auto-generated catch block
						return new NanoHTTPD.Response(Status.INTERNAL_ERROR, MIME_HTML, e.getMessage());
					}
					// se Url visionata è uguale a quella passata via parameter
					// non fare il restart Browser.
					if(!actualUrl.equalsIgnoreCase(newurl)) {
						Log.d(LOG_TAG,"Refreshing Url for Queue: "+newurl);
						restartBrowser();
					}
                }



			}else{
				return new NanoHTTPD.Response(Status.BAD_REQUEST,MIME_HTML,"KO");
			}
			return new NanoHTTPD.Response(Status.OK,MIME_HTML,"Success");


		}

		// Aggiornamento URL con dsMode=1
		if (uri.equalsIgnoreCase("/N3/updateMonitor")){
			String channel = parms.get("channel");
			if (null!=channel)
			{
				
				String newurl ="http://"+ prop.getProperty("ip-in-store")+channel;
				prop.setProperty("url",newurl);
				try {
					savePropertiesWithBackup();
                    restartBrowser();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());
				}
				
			}else{
				return new NanoHTTPD.Response(Status.BAD_REQUEST,MIME_HTML,"");
			}
			return new NanoHTTPD.Response(Status.OK,MIME_HTML,"Success");

		}

		// Chiude e riapre il Visualizzatore
		if ((uri.equalsIgnoreCase("/N3/device/executeScript") || uri.equalsIgnoreCase("/N3/remoteControl/executeScript") )
						&& (null!=parms.get("script") && parms.get("script").equalsIgnoreCase("restartBrowser") )
						){
			restartBrowser();
			setWallPaper();
			return new NanoHTTPD.Response(Status.OK,MIME_HTML,"");
		}


		if (null!= action && action.equalsIgnoreCase("shutdown")  ){

			AndroidExec ax = new AndroidExec();
			String[] commands = {"reboot -p"};
			ax.RunAsRoot(commands);
		}

		/*
		 * Reboot
		 * */
		

		if (null!= action && action.equalsIgnoreCase("reboot") ||
				((uri.equalsIgnoreCase("/N3/device/executeScript" ) || uri.equalsIgnoreCase("/N3/remoteControl/executeScript" ))
						&& (null!=parms.get("script") && parms.get("script").equalsIgnoreCase("reboot") )
						)){

			AndroidExec ax = new AndroidExec();
			String[] commands = {"reboot"};
			ax.RunAsRoot(commands);
		}

		/*
		 * Reset
		 * */
		if ((uri.equalsIgnoreCase("/N3/device/executeScript" ) || uri.equalsIgnoreCase("/N3/remoteControl/executeScript" ))
				&& (null!=parms.get("script") && parms.get("script").equalsIgnoreCase("reset") )
				){
		
					AndroidExec ax = new AndroidExec();
					String[] commands = {"rm /sdcard/local.prop","rm /sdcard/db.csv","echo \"ro.sf.hwrotation=0\\nro.sf.lcd_density=160\\nservice.adb.tcp.port=5555\" >/sdcard/local.prop","cp /sdcard/local.prop /data/local.prop ","rm /sdcard/config.properties","reboot"};
					Log.d(LOG_TAG,"comandi:"+commands.toString());
					AndroidExec axc = new AndroidExec();
					axc.RunAsRoot(commands);
		
				}


		if (uri.contains("/customUrl")) {

			if (null != parms.get("url")) {
				Log.d(LOG_TAG, "setting URL");

				prop.setProperty("url", parms.get("url"));
			}

			if (null != parms.get("autostart") && parms.get("autostart").equalsIgnoreCase("true")) {
				Log.d(LOG_TAG, "setting autostart");
				prop.setProperty("autostart", "true");

			} else if (null != parms.get("autostart") && parms.get("autostart").equalsIgnoreCase("false")) {
				//Log.d(LOG_TAG,"setting autostart");
				prop.setProperty("autostart", "false");
			}

			if (null != parms.get("audio") && parms.get("audio").equalsIgnoreCase("off")) {
				prop.setProperty("audio", "off");
				Log.d(LOG_TAG, "setting audio off");


			}

			if (null != parms.get("audio") && parms.get("audio").equalsIgnoreCase("on")) {
				prop.setProperty("audio", "on");
				Log.d(LOG_TAG, "setting audio on");

			}
			prop.setProperty("dsMode","2");
			// no fa mostare il prossimo step al CI e lo mettte in backGround all'avvio
			prop.setProperty("autostart", "true");
			prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
			requireRestart = true;

			try {
				savePropertiesWithBackup();



			} catch (IOException e) {
				return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());

			}


		}



		if (uri.contains(settingspath)){

			if  (null != parms.get("url")  )
			{
				Log.d(LOG_TAG,"setting URL");
				prop.setProperty("url", parms.get("url"));
			}

			if (null != parms.get("autostart") && parms.get("autostart").equalsIgnoreCase("true") ){
				Log.d(LOG_TAG,"setting autostart");
				prop.setProperty("autostart", "true");

			}else if (null != parms.get("autostart") && parms.get("autostart").equalsIgnoreCase("false"))
			{
				//Log.d(LOG_TAG,"setting autostart");
				//prop.setProperty("autostart", "false");
			}

			if (null != parms.get("audio")  && parms.get("audio").equalsIgnoreCase("off") ){
				prop.setProperty("audio", "off");
				Log.d(LOG_TAG,"setting audio off");
				
			
			}

			if (null != parms.get("audio")  && parms.get("audio").equalsIgnoreCase("on") ){
				prop.setProperty("audio", "on");
				Log.d(LOG_TAG,"setting audio on");
				
			}
			
			prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
			prop.setProperty("autostart", "true");
			
			
			
			
			
			requireRestart = true;
			
			try {
				savePropertiesWithBackup();
				
				
				
			} catch (IOException e) {
				return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());

			}
			

			//restart cisetingd
			
			
			Response res  = new NanoHTTPD.Response(Status.REDIRECT,MIME_HTML,"");
			res.addHeader( "Location", "/admin/"+defaultpage );
			return res;

		}


		//: method:GET uri:'/N3/device/executeScript' 
		// parms:{type=thin, script=screenshot, monitor=MONITOR_NAM}

		if (null!= action && action.equalsIgnoreCase("shot")
				|| ((uri.equalsIgnoreCase("/N3/device/executeScript" ) || uri.equalsIgnoreCase("/N3/remoteControl/executeScript" ))
						&& (null!=parms.get("script") && parms.get("script").equalsIgnoreCase("screenshot") ))){

			makeShotScreen();
			return new NanoHTTPD.Response(Status.OK,MIME_HTML,"");
		}

		if (null != parms.get("mode") && parms.get("mode").equalsIgnoreCase("landscape")){
			Log.d(LOG_TAG,"action landscape");
			prop.setProperty("mode", "landscape");
			prop.setProperty("rotation", "landscape");
			
			if (null != userAgent && userAgent.equalsIgnoreCase("n3client")) {
				prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#tabs-11");
			}else
			{
				prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
			}
			

			AndroidExec ax = new AndroidExec();
			String[] commands = {"rm /sdcard/local.prop","echo \"ro.sf.hwrotation=0\\nro.sf.lcd_density=160\nservice.adb.tcp.port=5555\" >/sdcard/local.prop","cp /sdcard/local.prop /data/local.prop "};
			Log.d(LOG_TAG, "LANDSCAPE");
			ax.RunAsRoot(commands);
			requireRestart = true;

			try {
				savePropertiesWithBackup();
			} catch (IOException e) {
				return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());

			}


			Response res  = new NanoHTTPD.Response(Status.REDIRECT,MIME_HTML,"");
			res.addHeader( "Location", "/admin/"+defaultpage+"?#landscape" );
			return res;

		}
		
		if (null != parms.get("mode") && parms.get("mode").equalsIgnoreCase("landscape2")){
			Log.d(LOG_TAG,"action landscape");
			prop.setProperty("mode", "landscape");
			prop.setProperty("rotation", "landscape2");
			if (null != userAgent && userAgent.equalsIgnoreCase("n3client")) {
				prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#tabs-11");
			}else
			{
				prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
			}
			

			AndroidExec ax = new AndroidExec();
			String[] commands = {"rm /sdcard/local.prop","echo \"ro.sf.hwrotation=180\\nro.sf.lcd_density=160\nservice.adb.tcp.port=5555\" >/sdcard/local.prop","cp /sdcard/local.prop /data/local.prop "};
			Log.d(LOG_TAG, "LANDSCAPE");
			ax.RunAsRoot(commands);
			requireRestart = true;

			try {
				savePropertiesWithBackup();
			} catch (IOException e) {
				return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());

			}

			Response res  = new NanoHTTPD.Response(Status.REDIRECT,MIME_HTML,"");
			res.addHeader( "Location", "/admin/"+defaultpage+"?#landscape" );
			return res;

		}


		if (null != parms.get("mode")  && parms.get("mode").equalsIgnoreCase("portrait")){
			Log.d(LOG_TAG,"action portrait");
			prop.setProperty("mode", "portrait");
			prop.setProperty("rotation", "portrait");
			if (null != userAgent && userAgent.equalsIgnoreCase("n3client")) {
				prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#tabs-11");
			}else
			{
				prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
			}
			AndroidExec ax = new AndroidExec();
			String[] commands = {"rm /sdcard/local.prop","echo \"ro.sf.hwrotation=270\\nro.sf.lcd_density=160\nservice.adb.tcp.port=5555\" >/sdcard/local.prop","cp /sdcard/local.prop /data/local.prop "};
			Log.d(LOG_TAG, "PORTRAIY");
			ax.RunAsRoot(commands);
			requireRestart = true;
			try {
				savePropertiesWithBackup();
			} catch (IOException e) {
				return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());

			}

			Response res  = new NanoHTTPD.Response(Status.REDIRECT,MIME_HTML,"");
			res.addHeader( "Location", "/admin/"+defaultpage+"?#portrait" );
			return res;
		}
		if (null != parms.get("mode")  && parms.get("mode").equalsIgnoreCase("portrait2")){
			Log.d(LOG_TAG,"action portrait");
			prop.setProperty("mode", "portrait");
			prop.setProperty("rotation", "portrait2");
			if (null != userAgent && userAgent.equalsIgnoreCase("n3client")) {
				prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#tabs-11");
			}else
			{
				prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#done");
			}
			AndroidExec ax = new AndroidExec();
			String[] commands = {"rm /sdcard/local.prop","echo \"ro.sf.hwrotation=90\\nro.sf.lcd_density=160\nservice.adb.tcp.port=5555\" >/sdcard/local.prop","cp /sdcard/local.prop /data/local.prop "};
			Log.d(LOG_TAG, "PORTRAIT");
			ax.RunAsRoot(commands);
			requireRestart = true;
			try {
				savePropertiesWithBackup();
			} catch (IOException e) {
				return new NanoHTTPD.Response(Status.INTERNAL_ERROR,MIME_HTML,e.getMessage());

			}

			Response res  = new NanoHTTPD.Response(Status.REDIRECT,MIME_HTML,"");
			res.addHeader( "Location", "/admin/"+defaultpage+"?#portrait" );
			return res;
		}



		Log.d(LOG_TAG,"Redirect to Admin Home");
		Response res  = new NanoHTTPD.Response(Status.REDIRECT,MIME_HTML,"");
		res.addHeader( "Location", "/admin/"+defaultpage+"?" );
		return res;

	}


	
	private synchronized void saveProperties() throws IOException
	{
		if (isExternalStorageWritable()){
			OutputStream output;
			output = new FileOutputStream(fileproperties);
			prop.store(output, null);
			output.flush();
			output.close();
			// se alcune propriet��� richiedono reboot
			if (requireRestart){
				String[] commands = {"reboot"};
				Log.d(LOG_TAG,"comandi:"+commands);
				AndroidExec axc = new AndroidExec();
				axc.RunAsRoot(commands); 
				Log.d(LOG_TAG,"fine copia");
				}
			
		} else throw new FileNotFoundException();

	}




	private synchronized void savePropertiesWithBackup() throws IOException
	{
		if (isExternalStorageWritable()){
			OutputStream output;
			output = new FileOutputStream(fileproperties);
			prop.store(output, null);
			output.flush();
			output.close();
			// se alcune propriet��� richiedono reboot
			if (requireRestart){
				String[] commands = {"cp "+fileproperties +" "+fileproperties+".bak","reboot"};
				Log.d(LOG_TAG,"comandi:"+commands);
				AndroidExec axc = new AndroidExec();
				axc.RunAsRoot(commands); 
				Log.d(LOG_TAG,"fine copia");
				}
			else
			{	
				String[] commands = {"cp "+fileproperties +" "+fileproperties+".bak"};
				Log.d(LOG_TAG,"comandi:"+commands);
				AndroidExec axc = new AndroidExec();
				axc.RunAsRoot(commands); 
				Log.d(LOG_TAG,"fine copia");
				
				
			}
		} else throw new FileNotFoundException();

	}

	// check network connection
	public boolean isConnected(){
		ConnectivityManager connMgr = (ConnectivityManager) activity.getSystemService(activity.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) 
			return true;
		else
			return false;   
	}


	// Ferma l'esecuzione del Viewer

	public void stopViewer(){

			AndroidExec ax = new AndroidExec();
			String[] commands = {"pkill -9 "+nameOfViewerPkg};
			ax.RunAsRoot(commands);



	}



/*
	public boolean isPackageRunning(String packagename) {
		return findPIDbyPackageName(packagename) != -1;
	}
	public int findPIDbyPackageName(String packagename) {
		int result = -1;

		if (am != null) {
			for (RunningAppProcessInfo pi : am.getRunningAppProcesses()){
				//Log.d("Http","pi.processName "+pi.processName);
				if (pi.processName.equalsIgnoreCase(packagename)) {
					//result = pi.pid;
					//android.os.Process.sendSignal(pi.pid, android.os.Process.SIGNAL_QUIT); 
					Log.d("Http","pi.processName "+pi.processName+" "+pi.pid);
					//android.os.Process.killProcess(pi.pid);
					return pi.pid;
				}
				if (result != -1) break;
			}
		} else {
			result = -1;
		}

		return result;
	}
*/


	/* Checks if external storage is available for read and write */
	public boolean isExternalStorageWritable() {
	/*	String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		}
		return false;
	*/
		return true;
	}

	
	/* Checks if external storage is available to at least read */
	public boolean isExternalStorageReadable() {
		/*String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state) ||
				Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return true;
		}
		return false;

		*/
	return true;
	}





	public void makeShotScreen() {

		AndroidExec ax = new AndroidExec();
		String[] commands = {"cd /sdcard"+fileshotpath,"rm "+nomeMonitor+".png","screencap -p "+nomeMonitor+".png"};
		ax.RunAsRoot(commands);


		float angle = 0;
        if (prop != null) {

            if (null != prop.getProperty("rotation") && prop.getProperty("rotation").equals("landscape2")) {
                angle = 180;
            }
            if (null != prop.getProperty("rotation") && prop.getProperty("rotation").equals("portrait2")) {
                angle = 270;
            }
            if (null != prop.getProperty("rotation") && prop.getProperty("rotation").equals("portrait")) {
                angle = 90;
            }


        }

        try {
		    Bitmap screen = RotateBitmap(resizeBitmap(140, 80, "/sdcard"+fileshotpath+nomeMonitor+".png"),angle);
		
		    File file = new File ("/sdcard"+fileshotpath+nomeMonitor+"_tb.png");
		    if (file.exists ()) file.delete ();

                FileOutputStream out = new FileOutputStream(file);
                screen.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.flush();
                out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// Img tumb per shot
	public Bitmap resizeBitmap(int targetW, int targetH,String photoPath) {
		return Bitmap.createScaledBitmap(BitmapFactory.decodeFile(photoPath), targetW, targetH, false);
		
		//return ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(photoPath), targetW, targetH);          
	}
	public static Bitmap RotateBitmap(Bitmap source, float angle)
	{
	      Matrix matrix = new Matrix();
	      matrix.postRotate(angle);
	      return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}

	 class PInfo {
		    private String appname = "";
		    private String pname = "";
		    private String versionName = "";
		    private int versionCode = 0;
		
		    public String getAppname() {
				return appname;
			}

			public void setAppname(String appname) {
				this.appname = appname;
			}

			public String getPname() {
				return pname;
			}

			public void setPname(String pname) {
				this.pname = pname;
			}

			public String getVersionName() {
				return versionName;
			}

			public void setVersionName(String versionName) {
				this.versionName = versionName;
			}

			public int getVersionCode() {
				return versionCode;
			}

			public void setVersionCode(int versionCode) {
				this.versionCode = versionCode;
			}

			
		}
	 

		private String getPackages() {
		    ArrayList<PInfo> apps = getInstalledApps(false); /* false = no system packages */
		    final int max = apps.size();
		    String jsonresp = "[";
		    for (int i=0; i<max; i++) {
		        Log.d(LOG_TAG, jsonresp);
		        if (apps.get(i).getPname().contains("kodobeat")){
		        	jsonresp+="{\"appname\":\""+apps.get(i).getAppname()+"\","
		        			+ "\"pname\":\""+apps.get(i).getPname()+"\","
		        			+ "\"versionName\":\""+apps.get(i).getVersionName()+"\"},";
		        }
		    }
		    jsonresp = jsonresp.substring(0, jsonresp.length()-1);
		    jsonresp+="]";
		    return jsonresp;
		}

		private ArrayList<PInfo> getInstalledApps(boolean getSysPackages) {
		    ArrayList<PInfo> res = new ArrayList<PInfo>();        
		    List<PackageInfo> packs = activity.getPackageManager().getInstalledPackages(0);
		    for(int i=0;i<packs.size();i++) {
		        PackageInfo p = packs.get(i);
		        if ((!getSysPackages) && (p.versionName == null)) {
		            continue ;
		        }
		        PInfo newInfo = new PInfo();
		        newInfo.appname = p.applicationInfo.loadLabel(activity.getPackageManager()).toString();
		        newInfo.pname = p.packageName;
		        newInfo.versionName = p.versionName+"."+p.versionCode;
		        newInfo.versionCode = p.versionCode;
		       
		        res.add(newInfo);
		    }
		    return res; 
		}
	 



}
