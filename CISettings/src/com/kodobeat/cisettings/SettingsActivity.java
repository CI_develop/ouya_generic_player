package com.kodobeat.cisettings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.util.Properties;


import com.kodobeat.CISettings;
import com.kodobeat.util.AndroidExec;
import com.kodobeat.util.N3ClientNetwokInfo;
import com.kodobeat.util.UpdateCSV;

import android.app.ActionBar;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/// boot http://stackoverflow.com/questions/6391902/how-to-start-an-application-on-startup
public class SettingsActivity extends Activity  {
	protected PowerManager.WakeLock mWakeLock;
	private static final String LOG_TAG = "SettingsActivity";
	private CISettings server,serverlocal;
	private static Context context;

	private static String nameOfViewerPkg = "it.mediamarket.instoretablet";

	private static String nameOfClass = "com.vipera.dynamicengine.DESplashActivity";
	private Properties prop;
	private static String fileproperties = "/sdcard/config.properties";



	public static Context getAppContext() {
		return SettingsActivity.context;
	}

	// Imposta la connessione ip statica

	private void setStaticNetwork(String netInterface){

		String ipstatic = 	prop.getProperty("ip");
		String netmaskstatic =	prop.getProperty("netmask");
		String gatewaystatic =	prop.getProperty("gateway");
		String dns1static =	prop.getProperty("dns1");
		String dns2static =	prop.getProperty("dns2");
		String hostname =	prop.getProperty("hostname");


		if (netInterface.equalsIgnoreCase("eth0")){

			//"mount -o remount,rw /system",
			//"chmod 444 /system/bin/dhcpcd",
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        	{
				Log.d(LOG_TAG,"Network for KITKAT ");
				String[] commands = {
						"pkill -9 dhcpcd",
						"mount -o remount,rw /system",
						"chmod 444 /system/bin/dhcpcd",
						"ifconfig "+netInterface+" "+ipstatic +" netmask "+netmaskstatic,
						"route add default gw "+gatewaystatic +" dev "+netInterface,
						"setprop net.hostname "+hostname,
						"ndc resolver flushif "+netInterface,
						"ndc resolver flushdefaultif",
						"ndc resolver setifdns "+netInterface+" "+dns1static+" "+dns2static,
						"ndc resolver setdefaultif "+netInterface
				};
				AndroidExec axc_net = new AndroidExec();
				axc_net.RunAsRoot(commands);

			}




		}

		
	}
	// Imposta la connessione ip statica
	private void setDynamicNetwork(String netInterface){

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

			String[] commands = {
					"mount -o remount,rw /system",
					"chmod 755 /system/bin/dhcpcd",
					"/system/bin/dhcpcd " + netInterface
			};

			AndroidExec axc_net = new AndroidExec();
			axc_net.RunAsRoot(commands);
		} else
		{    //board 6309 MM
			String[] commands = {
					"mount -o remount,rw /system",
					"rm /persist/eth0.sh"
			};

			AndroidExec axc_net = new AndroidExec();
			axc_net.RunAsRoot(commands);
		}
	}




	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "");
		this.mWakeLock.acquire();

		String[] commands_overlay = {"service call SurfaceFlinger 1008 i32 1"};
		Log.d(LOG_TAG,"Settinfg HW overlay OFF");
		AndroidExec axc_hw = new AndroidExec();
		axc_hw.RunAsRoot(commands_overlay); 


		String url ="";
		SettingsActivity.context = getApplicationContext();

		//String fileproperties = Environment.getExternalStorageDirectory().getAbsolutePath()+"/config.properties";

		Log.d(LOG_TAG,"fileproperties :"+fileproperties);
		prop = new Properties();

		try {
			prop.load(new FileInputStream(fileproperties));

			if (!checkPropValidity()){
				Log.e(LOG_TAG, "Properties FILE contain ERROR trying to load BAK");
				prop.clear(); // cancello il vecchio contenuto
				prop.load(new FileInputStream(fileproperties + ".bak"));
				if (!checkPropValidity()){
					Log.e(LOG_TAG, "Properties FILE BAK contain ERROR trying to load DEFAULT");
					saveDefaultProperties();
					prop.clear();
					prop.load(new FileInputStream(fileproperties));
				}else
				{
					restoreBackup();
				}
				
			}





			// S Modifiche Inforce 6309
			if (null!=prop && null!=prop.getProperty("mode") && prop.getProperty("mode").equalsIgnoreCase("landscape"))
			{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

			} else{

				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}


			// E Modifiche Inforce 6309




			//Gestione dell' Audio al boot del Settings
			if (null!=prop && null!=prop.getProperty("audio") && prop.getProperty("audio").equalsIgnoreCase("on"))
			{
				Log.d(LOG_TAG,"setting audio on");
				AudioManager mgr = (AudioManager) getSystemService(context.AUDIO_SERVICE);
				int maxVolume = mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
				mgr.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, AudioManager.FLAG_PLAY_SOUND);
				mgr.setStreamVolume(AudioManager.STREAM_RING, maxVolume, AudioManager.FLAG_PLAY_SOUND);
				mgr.setStreamVolume(AudioManager.STREAM_ALARM, maxVolume, AudioManager.FLAG_PLAY_SOUND);
				mgr.setStreamVolume(AudioManager.STREAM_SYSTEM, maxVolume, AudioManager.FLAG_PLAY_SOUND);
				mgr.setStreamVolume(AudioManager.STREAM_NOTIFICATION, maxVolume, AudioManager.FLAG_PLAY_SOUND);

			}
			else
			{
				Log.d(LOG_TAG,"setting audio off");
				//scrive solo nel file di properties e muteoff
				AudioManager mgr=(AudioManager) getSystemService(context.AUDIO_SERVICE);
				mgr.setStreamVolume(AudioManager.STREAM_MUSIC, 0, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
				mgr.setStreamVolume(AudioManager.STREAM_RING, 0, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
				mgr.setStreamVolume(AudioManager.STREAM_ALARM, 0, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
				mgr.setStreamVolume(AudioManager.STREAM_SYSTEM, 0, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
				mgr.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 0, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);

			}


			/*	
			 * IF PROPERTIES STATIC FORCE IP
			 */	

			String ipListen="127.0.0.1";
			//String interfacename ="eth0";
			String interfacename = prop.getProperty("netinterface","eth0");
			if (null == interfacename) {interfacename = "eth0";} // imposto wired come default
			
			if (interfacename.equalsIgnoreCase("wlan0")){
				// ATTIVO WIFI
				WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
				wifiManager.setWifiEnabled(true);


				String ssid =	prop.getProperty("SSID-name", "SSID");
				String ssidsecret = prop.getProperty("SSID-secret", "SSIDpwd");


			
				
				WifiConfiguration wifiConfig = new WifiConfiguration();
				wifiConfig.SSID = String.format("\"%s\"", ssid);
				wifiConfig.preSharedKey = String.format("\"%s\"", ssidsecret);
				
				wifiConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
				wifiConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
				wifiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
				wifiConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
				wifiConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
				wifiConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
			
				if (null!=prop && 
					null!=prop.getProperty("ipstatic") &&
					prop.getProperty("ipstatic").equalsIgnoreCase("true"))
					{
					
					Log.d("WIFI ", "Set IP STATIC");
					
					String ipstatic = 	prop.getProperty("ip");
					String netmaskstatic =	prop.getProperty("netmask");
					String gatewaystatic =	prop.getProperty("gateway");
					String dns1static =	prop.getProperty("dns1");
					String dns2static =	prop.getProperty("dns2");
						
						 try{
							 	wifiManager.removeNetwork(0);
							 	wifiManager.removeNetwork(1);
						        setIpAssignment("STATIC", wifiConfig); //or "DHCP" for dynamic setting
						        setIpAddress(InetAddress.getByName(ipstatic), 24, wifiConfig);
						        setGateway(InetAddress.getByName(gatewaystatic), wifiConfig);
						        setDNS(InetAddress.getByName(dns1static),InetAddress.getByName(dns2static) ,wifiConfig);
						        wifiManager.updateNetwork(wifiConfig); //apply the setting
						        wifiManager.saveConfiguration(); //Save it
						    }catch(Exception e){
						        e.printStackTrace();
						        Log.e("WIFI ", e.getMessage());  
						    }
						
						 
						 /*
						 //remember id
							
						*/	
						
					}
				else
					{
					Log.d("WIFI ", "Set IP DINAMIC");
						 try{
							 	wifiManager.removeNetwork(0);
							 	wifiManager.removeNetwork(1);
						        setIpAssignment("DHCP", wifiConfig); //or "DHCP" for dynamic setting
						        wifiManager.updateNetwork(wifiConfig); //apply the setting
						        wifiManager.saveConfiguration(); //Save it
						    }catch(Exception e){
						        e.printStackTrace();
						        Log.e("WIFI ", e.getMessage());  
						    }
						
					}

				int netId = wifiManager.addNetwork(wifiConfig);
				Log.d("WIFI ", "Current NetID ["+netId+"]");
				wifiManager.disconnect();
				wifiManager.enableNetwork(netId, true);
				wifiManager.reconnect();
			
				
				
			}
			else{
				// DISATTIVO WIFI
				WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
				wifiManager.setWifiEnabled(false);

			}



			//se il è impostato ip statico mi metto in listen su ip scritto in conf
			if (null!=prop && 
				null!=prop.getProperty("ipstatic") &&
			prop.getProperty("ipstatic").equalsIgnoreCase("true"))
			{
				
				setStaticNetwork(interfacename);
				ipListen = prop.getProperty("ip");

			} else
			{
				//DHCP cerca di ottenere un IP
				setDynamicNetwork(interfacename);
				N3ClientNetwokInfo info = new N3ClientNetwokInfo();
				ipListen =  info.getIPbydev(interfacename);
				int countfailed = 0;

				while (ipListen.isEmpty())
				{
					if (countfailed >3 ){
						// reboot
						AndroidExec ax = new AndroidExec();
						String[] commands = {"reboot"};
						ax.RunAsRoot(commands);
						
					}
					
					ipListen =  info.getIPbydev(interfacename);
					Log.d(LOG_TAG, "check ip: on "+interfacename+" ip ="+ipListen);
					try {
						Thread.sleep(6000);
						Log.d(LOG_TAG, "countfailed "+countfailed);
						countfailed++;
						//???
						setDynamicNetwork(interfacename);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						ipListen = "127.0.0.2";
					}
				}//end while
			}

			
			
			serverlocal = new CISettings(this,"127.0.0.1");
			serverlocal.start();


			Log.d(LOG_TAG, "Starting...on "+interfacename+" ip ="+ipListen);
			server = new CISettings(this,ipListen);
			server.start();


			Log.d(LOG_TAG, "autostart "+prop.getProperty("autostart"));

			if (null!= prop && null!=prop.getProperty("autostart") && prop.getProperty("autostart").equalsIgnoreCase("true")){


				if (!isPackageRunning(nameOfViewerPkg)){
					Log.d(LOG_TAG,"isPackageRunning " +nameOfViewerPkg);

					// Auto start viewer only in not running
					Intent in = new Intent(Intent.ACTION_MAIN);
					in.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
					in.addCategory(Intent.CATEGORY_LAUNCHER);
					in.putExtra("url", "#");
					in.setClassName(nameOfViewerPkg, nameOfClass); 
					context.startActivity(in);  
				}
			}



		} 
		catch (FileNotFoundException e) {
			
			/* Non trova il file di configurazione
			 * quindi siamo nella prima fase iniziale ed impostiamo il network su loopback 127.0.0.1
			 */	
			

			
			Log.e(LOG_TAG,"First BootStrap config.properties does not exist!");
			Log.d(LOG_TAG, "Starting...on  127.0.0.1");
			url ="http://127.0.0.1:8080/default.html?#tabs-12";
			try {
				saveDefaultProperties();
				server = new CISettings(this,"127.0.0.1");
				server.start();
				

			} catch (Exception e1) {
				// TODO Auto-generated catch block
				Log.e(LOG_TAG, e1.getMessage());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(LOG_TAG, e.getMessage());
			// Qui potrebbe entrare se il viewer non esiste o non parte 
			Log.e(LOG_TAG, "Somethings goes wrong! line 281");
		}

		// Ricordarsi di creaare la dir per la mem delle img
		setContentView(R.layout.activity_settings);
		///

		final WebView webView = (WebView) findViewById(R.id.webView1);
		final WebSettings settings = webView.getSettings();
		settings.setJavaScriptEnabled(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		settings.setPluginState(WebSettings.PluginState.ON);
		settings.setDomStorageEnabled(true);
		settings.setLoadsImagesAutomatically(true);
		settings.setSupportMultipleWindows(false);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(false);
		settings.setSupportZoom(false);
		settings.setUserAgentString("n3client");



		webView.setWebViewClient(new WebViewClient() {
			// autoplay when finished loading via javascript injection
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				//webView.loadUrl("javascript:(function() { document.getElementsByTagName('video')[0].play();document.getElementsByTagName('video')[0].loop =true;})()"); 
			}


		});
		webView.setClickable(true);
		webView.setFocusableInTouchMode(true);
		webView.setWebChromeClient(new WebChromeClient() {});

		url = prop.getProperty("nexturl");
		Log.d(LOG_TAG, "nexturl "+url);


		webView.loadUrl(url);
		// ultimo controllo ... 
		// se esiste il file una chiave usb montata allor mosta
		//la web view sul tab si seleziona csv
		// mi riduco in ascolto nel Task Bar
		if (url.equalsIgnoreCase("http://127.0.0.1:8080/default.html?#done")){
			//moveTaskToBack (true);
			finish();
		}

	}


	private Boolean isConfigBakExsist() {
		File file = new File(fileproperties+".bak");
		return file.exists();
	}

	private void restoreBackup()
	{
		AndroidExec save = new AndroidExec();
		// Recupero la configurazione
		if (isConfigBakExsist()){

			String[] savecommands = {"cp "+fileproperties+".bak "+fileproperties};
			save.RunAsRoot(savecommands);
		} 
		else{
			try {
				saveDefaultProperties();
				String[] savecommands = {"cp "+fileproperties+" "+fileproperties+".bak"};
				save.RunAsRoot(savecommands);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/*
	 * Crea il primo file di configurazione (Default)
	 * */

	private void saveDefaultProperties() throws IOException
	{

		OutputStream output;
		output = new FileOutputStream(fileproperties);
		prop.clear();
		prop.setProperty("nexturl", "http://127.0.0.1:8080/default.html?#tabs-12");
		prop.setProperty("audio", "off");
		prop.setProperty("autostart", "false");
		prop.setProperty("netmask", "255.255.255.0");
		prop.setProperty("ip", "127.0.0.1");
		prop.setProperty("gateway", "127.0.0.1");
		prop.setProperty("dns1", "127.0.0.1");
		prop.setProperty("dns2", "127.0.0.1");
		prop.setProperty("ipstatic", "true");
		prop.setProperty("mode", "landscape");
		prop.setProperty("rotation", "landscape");
		prop.setProperty("settingversion", "0");
		prop.setProperty("viewerversion", "0");
		prop.setProperty("baseurl", "/");
		prop.setProperty("instoreip_backip", "127.0.0.1");
		prop.setProperty("ip-in-store", "127.0.0.1");
		prop.setProperty("ip-in-store-list", "127.0.0.1:8080|127.0.0.1:8080");
		prop.setProperty("activeUrl","127.0.0.1");
		prop.setProperty("storecode", "000");
		prop.setProperty("url", "http://127.0.0.1:8080/default.html");
		prop.setProperty("hostname","thinclient");
		prop.setProperty("monitor-name", "thinclient");
		prop.setProperty("brand", "CI");
		prop.setProperty("netcheck-intervall", "10");
		prop.setProperty("interface-name", "eth0");
		prop.setProperty("SSID-name", "SSID");
		prop.setProperty("SSID-secret", "SSIDpwd");
		prop.setProperty("cache-browser", "50");
		prop.setProperty("shot-intervall", "120");
		prop.setProperty("netcheck-intervall", "10");
		prop.setProperty("netinterface","eth0");
		prop.setProperty("urljsoncontent", "http://127.0.0.1:8080/N3");
		prop.getProperty("dsMode","1");


		prop.store(output, null);
		output.flush();
		output.close();



	}

	/*
	 * Controlla che sia possibile legggere tutti i valori
	 * */
	private boolean checkPropValidity(){

	int currentcheck = 0;
	int NUMER_PROPS = 28;
	
		try{
			if (prop.getProperty("nexturl").startsWith("http")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("audio").startsWith("o")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("autostart").contains("true") || prop.getProperty("autostart").contains("false")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("netmask").startsWith("255")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("ip").contains(".")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("instoreip_backip").contains(".")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("gateway").contains(".")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("dns1").contains(".")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("dns2").contains(".")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("ipstatic").contains("true") || prop.getProperty("ipstatic").contains("false")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("ip-in-store").contains(".")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("activeUrl").contains(".")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("url").startsWith("http")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (prop.getProperty("interface-name").contains("eth0") || prop.getProperty("interface-name").contains("wlan0")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("mode")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("rotation")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("settingversion")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("viewerversion")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("storecode")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("hostname")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("monitor-name")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("brand")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("netcheck-intervall")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("SSID-name")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("SSID-secret")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("cache-browser")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("shot-intervall")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			if (null != prop.getProperty("netcheck-intervall")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			// Lo escudo dal controllo
			//if (null != prop.getProperty("ip-in-store-list")) {Log.d(LOG_TAG,"check OK "+currentcheck++);}
			
			else {Log.d(LOG_TAG,"check FAILED");currentcheck = 0;}
		}catch (Exception e){
			Log.d(LOG_TAG,"check FAILED on "+e.getMessage());
			currentcheck = 0;

		}
		Log.d(LOG_TAG,"check sum "+currentcheck);
		if (currentcheck == NUMER_PROPS)
			return true;
		else
			return false;

	}

	public boolean isPackageRunning(String packagename) {
		if (findPIDbyPackageName(packagename)!= -1) {
			Log.d("Http",packagename +" isRunning");
			return true;

		}
			else{
			Log.d("Http",packagename +" is NOT Running");
			return false;

		}


	}
	public int findPIDbyPackageName(String packagename) {
		int result = -1;
		ActivityManager am  = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		if (am != null) {
			for (RunningAppProcessInfo pi : am.getRunningAppProcesses()){
				Log.d("*Http","pi.processName "+pi.processName);
				if (pi.processName.equalsIgnoreCase(packagename)) {
					//result = pi.pid;
					//android.os.Process.sendSignal(pi.pid, android.os.Process.SIGNAL_QUIT); 
					Log.d("Http","pi.processName "+pi.processName+" with PID "+pi.pid);
					//android.os.Process.killProcess(pi.pid);
					result = pi.pid;
					break;
					//return result;
				}
				if (result != -1) break;
			}
		} else {
			result = -1;
		}

		return result;
	}




	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		Log.d(LOG_TAG, "TASTO keyCode=" +keyCode);
		if (keyCode == 4)
		{
			Log.d(LOG_TAG, "ESC");
			return super.onKeyDown(5, event);
			//onESCPressed();
		}
		return super.onKeyDown(keyCode, event);
	}





	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);

		return true;
	}
	
	
	//WIFI
	public static void setIpAssignment(String assign, WifiConfiguration wifiConf)
			throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Object ipConfiguration = wifiConf.getClass().getMethod("getIpConfiguration").invoke(wifiConf);
			setEnumField(ipConfiguration, assign, "ipAssignment");
		} else {
			setEnumField(wifiConf, assign, "ipAssignment");
		}
	}

			    public static void setIpAddress(InetAddress addr, int prefixLength, WifiConfiguration wifiConf)
			    throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException,
			    NoSuchMethodException, ClassNotFoundException, InstantiationException, InvocationTargetException{
			        Object linkProperties = getField(wifiConf, "linkProperties");
			        if(linkProperties == null)return;
			        Class laClass = Class.forName("android.net.LinkAddress");
			        Constructor laConstructor = laClass.getConstructor(new Class[]{InetAddress.class, int.class});
			        Object linkAddress = laConstructor.newInstance(addr, prefixLength);

			        ArrayList mLinkAddresses = (ArrayList)getDeclaredField(linkProperties, "mLinkAddresses");
			        mLinkAddresses.clear();
			        mLinkAddresses.add(linkAddress);        
			    }

			    public static void setGateway(InetAddress gateway, WifiConfiguration wifiConf)
			    throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException, 
			    ClassNotFoundException, NoSuchMethodException, InstantiationException, InvocationTargetException{
			        Object linkProperties = getField(wifiConf, "linkProperties");
			        if(linkProperties == null)return;
			        Class routeInfoClass = Class.forName("android.net.RouteInfo");
			        Constructor routeInfoConstructor = routeInfoClass.getConstructor(new Class[]{InetAddress.class});
			        Object routeInfo = routeInfoConstructor.newInstance(gateway);

			        ArrayList mRoutes = (ArrayList)getDeclaredField(linkProperties, "mRoutes");
			        mRoutes.clear();
			        mRoutes.add(routeInfo);
			    }

			    public static void setDNS(InetAddress dns1,InetAddress dns2, WifiConfiguration wifiConf)
			    throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException{
			        Object linkProperties = getField(wifiConf, "linkProperties");
			        if(linkProperties == null)return;

			        ArrayList<InetAddress> mDnses = (ArrayList<InetAddress>)getDeclaredField(linkProperties, "mDnses");
			        mDnses.clear(); //or add a new dns address , here I just want to replace DNS1
			        mDnses.add(dns1);
			        mDnses.add(dns2);
			        
			    }

			    public static Object getField(Object obj, String name)
			    throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
			        Field f = obj.getClass().getField(name);
			        Object out = f.get(obj);
			        return out;
			    }

			    public static Object getDeclaredField(Object obj, String name)
			    throws SecurityException, NoSuchFieldException,
			    IllegalArgumentException, IllegalAccessException {
			        Field f = obj.getClass().getDeclaredField(name);
			        f.setAccessible(true);
			        Object out = f.get(obj);
			        return out;
			    }  

			    public static void setEnumField(Object obj, String value, String name)
			    throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
			        Field f = obj.getClass().getField(name);
			        f.set(obj, Enum.valueOf((Class<Enum>) f.getType(), value));
			    }
	//END WIFI

}
