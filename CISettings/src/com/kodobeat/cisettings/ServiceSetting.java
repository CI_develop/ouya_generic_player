package com.kodobeat.cisettings;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;

import com.kodobeat.CISettings;
import com.kodobeat.util.AndroidExec;
import com.kodobeat.util.N3ClientNetwokInfo;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

/**
 * Created by marco on 12/04/17.
 */

public class ServiceSetting   extends Service {
    private static final String LOG_TAG = "ServiceSetting";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //Creating new thread for my service
        //Always write your long running tasks in a separate thread, to avoid ANR
        Log.d(LOG_TAG,"onStartCommand");
        new SettingyAsync().execute();
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    class SettingyAsync extends AsyncTask<Void, Integer, String>
    {

        PowerManager.WakeLock mWakeLock;


        private  String nameOfClass = "com.vipera.dynamicengine.DESplashActivity";
        private Properties prop;
        private  String fileproperties = "/sdcard/config.properties";
        protected void onPreExecute (){
            final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "");
            this.mWakeLock.acquire();


        }



        protected String doInBackground(Void...arg0) {


        try{

                Log.d(LOG_TAG,"doInBackground");
                N3ClientNetwokInfo info = new N3ClientNetwokInfo();
                CISettings server = new CISettings(getApplicationContext(),info.getIPbydev("eth0"));

                server.start();





            }catch ( Exception e){
                e.printStackTrace();

            }



        /*   while (true) {




                try {

                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        */

            return  "OK";
        }

        protected void onProgressUpdate(Integer...a){

        }

        protected void onPostExecute(String result) {

        }
    }
}
