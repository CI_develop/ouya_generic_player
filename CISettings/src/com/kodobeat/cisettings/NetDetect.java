package com.kodobeat.cisettings;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.kodobeat.util.AndroidExec;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

public class NetDetect extends BroadcastReceiver {

	private static final String LOG_TAG = "NetDetect";
	private static String fileproperties = "/sdcard/config.properties";
	private static void closeInputStream(InputStream is) {
		try {
			if (is != null) {
				is.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		  Log.d(LOG_TAG,"Lan/wan Change State ");

		  ConnectivityManager cm =
			        (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		  
		  boolean isConnected = activeNetwork != null &&
                  activeNetwork.isConnectedOrConnecting();
		  Log.d(LOG_TAG,"Lan/wan isConnected  "+isConnected);

		  	Properties prop;
			prop = new Properties();
			InputStream propIn = null;
			try {
				propIn = new FileInputStream(fileproperties);
				prop.load(propIn);
				if (null!=prop && null!=prop.getProperty("ipstatic") && prop.getProperty("ipstatic").equalsIgnoreCase("true")){

					String ipstatic = 	prop.getProperty("ip");
					String netmaskstatic =	prop.getProperty("netmask");
					String gatewaystatic =	prop.getProperty("gateway");
					String dns1static =	prop.getProperty("dns1");
					String dns2static =	prop.getProperty("dns2");
					String hostname =	prop.getProperty("hostname");
					String interfacename = prop.getProperty("netinterface");

					if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)

					{
                        Log.d(LOG_TAG,"Network for KITKAT ");
						String[] commands = {
								"pkill -9 dhcpcd",
								"mount -o remount,rw /system",
								"chmod 444 /system/bin/dhcpcd",
								"ifconfig "+interfacename+" "+ipstatic +" netmask "+netmaskstatic,
								"route add default gw "+gatewaystatic +" dev "+interfacename,
								"setprop net.hostname "+hostname,
								"ndc resolver flushif "+interfacename,
								"ndc resolver flushdefaultif",
								"ndc resolver setifdns "+interfacename+" "+dns1static+" "+dns2static,
								"ndc resolver setdefaultif "+interfacename
						};
						AndroidExec axc_net = new AndroidExec();
						axc_net.RunAsRoot(commands);

					}


				} 
			}catch (Exception e) {
				// TODO: handle exception
			}finally {
				closeInputStream(propIn);
			}

	}


}
