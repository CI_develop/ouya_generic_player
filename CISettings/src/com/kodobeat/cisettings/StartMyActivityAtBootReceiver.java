package com.kodobeat.cisettings;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.kodobeat.CISettings;

public class StartMyActivityAtBootReceiver extends BroadcastReceiver {


	private static final String LOG_TAG = "CIBootReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {



    	Intent i = new Intent(context, SettingsActivity.class);
	    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    context.startActivity(i);

        SetAlarm(context);


		
	}

	public void SetAlarm(Context context)
	{
		AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);



        Intent settingService = new Intent(context, ServiceSetting.class);

		PendingIntent pi = PendingIntent.getService(context, 0, settingService, 0);
		//After after 5 seconds
		am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 *60  , pi);
	}

	public void CancelAlarm(Context context)
	{
		Intent intent = new Intent(context, StartMyActivityAtBootReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
	}


}