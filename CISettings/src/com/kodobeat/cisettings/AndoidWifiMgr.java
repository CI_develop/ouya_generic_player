package com.kodobeat.cisettings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

public class AndoidWifiMgr extends BroadcastReceiver {
	private static final String LOG_TAG = "WIFI_MANAGER";
	private  ArrayList<HashMap<String, String>> arraylist = new ArrayList<HashMap<String, String>>();
	private boolean readSSID = false; 
	
	
	

	public boolean isReadSSID() {
		return readSSID;
	}


	public void setReadSSID(boolean readSSID) {
		this.readSSID = readSSID;
		//if (readSSID)
		//	arraylist.clear();
	}


	public ArrayList<HashMap<String, String>> getArraylist() {
		return arraylist;
	}


	public void setArraylist(ArrayList<HashMap<String, String>> arraylist) {
		this.arraylist = arraylist;
	}


	public  AndoidWifiMgr() {
		
		  Log.d(LOG_TAG,"WIFI AndoidWifiMgr");
		  
	}
	
	
	@Override
	public void onReceive(Context context, Intent intent) {
		  Log.d(LOG_TAG,"Receiver onReceive "+intent.getAction());
		
		
		  if(intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION) && readSSID){
			
			 
			  WifiManager w = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);


	            List<ScanResult> l = w.getScanResults();
	            
		        for(int i=0;i<l.size();i++){
		        	HashMap<String, String> item = new HashMap<String, String>(); 
		            Log.i("Wifi SSID",l.get(i).level+" "+l.get(i).BSSID+" "+l.get(i).SSID);
		            item.put(l.get(i).SSID, l.get(i).BSSID);
	                 arraylist.add(item);
		            
		        }

		     }
		  
		
        
		
	}
	
}


