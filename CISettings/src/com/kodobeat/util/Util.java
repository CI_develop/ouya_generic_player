package com.kodobeat.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

public class Util {

	public static boolean assetExists(AssetManager assets, String name) {
	    try {
	        // using File to extract path / filename
	        // alternatively use name.lastIndexOf("/") to extract the path
	        File f = new File(name);
	        String parent = f.getParent();
	        if (parent == null) parent = "";
	        String fileName = f.getName();
	        // now use path to list all files
	        String[] assetList = assets.list(parent);
	        if (assetList != null && assetList.length > 0) {
	            for (String item : assetList) {
	                if (fileName.equals(item))
	                    return true;
	            }
	        }
	    } catch (IOException e) {
	        // Log.w(TAG, e); // enable to log errors
	    }
	    return false;
	}
	
	public static void closeInputStream(InputStream is) {
		try {
			if (is != null) {
				is.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	
	public static boolean isPackageRunning(String packagename, Context ctx) {
		return findPIDbyPackageName(packagename, ctx) != -1;
	}
	
	private static  int findPIDbyPackageName(String packagename,Context ctx) {
		int result = -1;
		ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
		if (am != null) {
			for (RunningAppProcessInfo pi : am.getRunningAppProcesses()){
				Log.d("CIProcess","Check pi.processName "+pi.processName +"<>" +packagename);
				if (pi.processName.equalsIgnoreCase(packagename)) {
					//result = pi.pid;
					//android.os.Process.sendSignal(pi.pid, android.os.Process.SIGNAL_QUIT); 
					Log.d("CIProcess","Fired pi.processName "+pi.processName+" "+pi.pid);
					//android.os.Process.killProcess(pi.pid);

					if (pi.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && pi.processName.equals(packagename)) {
						return pi.pid;
					}
					return pi.pid;
				}
				//if (result != -1) break;
			}
		} else {
			result = -1;
		}
		Log.d("CIProcess","result " +result);
		return result;
	}


}
