package com.kodobeat.util;

import java.io.Serializable;
import java.util.ArrayList;

public class Thin implements Serializable {

	private String brand;
	private String codice;
	private String cardholderEmail;
	private String Hostname;
	private String citta;
	private String ip;
	private String gateway;
	private String sottorete;
	private String dns1;
	private String dns2;
	private String instoreip;
	private String instoreip_backup;
	private String monitor_name;
	
	
	
	public String getMonitor_name() {
		if (null!= monitor_name)
		return monitor_name;
		else
			return "\"\"";	
	}
	public void setMonitor_name(String monito_name) {
		this.monitor_name = monito_name;
	}
	public String getInstoreip_backup() {
		return instoreip_backup;
	}
	public void setInstoreip_backup(String instoreip_backup) {
		this.instoreip_backup = instoreip_backup;
	}
	private String howmany;
	
	public String getHowmany() {
		return howmany;
	}
	public void setHowmany(String howmany) {
		this.howmany = howmany;
	}
	public String getInstoreip() {
		return instoreip;
	}
	public void setInstoreip(String instoreip) {
		this.instoreip = instoreip;
	}
	private int index;
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getCardholderEmail() {
		return cardholderEmail;
	}
	public void setCardholderEmail(String cardholderEmail) {
		this.cardholderEmail = cardholderEmail;
	}
	public String getHostname() {
		return Hostname;
	}
	public void setHostname(String hostname) {
		Hostname = hostname;
	}
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	public String getSottorete() {
		return sottorete;
	}
	public void setSottorete(String sottorete) {
		this.sottorete = sottorete;
	}
	public String getDns1() {
		return dns1;
	}
	public void setDns1(String dns1) {
		this.dns1 = dns1;
	}
	public String getDns2() {
		return dns2;
	}
	public void setDns2(String dns2) {
		this.dns2 = dns2;
	}
	
	
	

}
