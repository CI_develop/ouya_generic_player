package com.kodobeat.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import 	android.util.Base64;
import android.util.Log;

public class UpdateCSV {
	
	private static final String LOG_TAG = "UpdateCSV";

	public static boolean checkHeadercsv(String filecsv)
	{
		String header ="Brand;Codice Negozio;Hostname;Citta;Indirizzo IP;Gateway;Sottorete;DNS PRI;DNS SEC;instore IP1;instore IP2;Monitor Name";
		String masterMD5="";
		try {
			byte[] bytesOfMessage = header.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(bytesOfMessage);
			 masterMD5 = Base64.encodeToString(thedigest,Base64.DEFAULT);
			
		} catch (UnsupportedEncodingException e1) {
			Log.d(LOG_TAG, "UnsupportedEncodingException "+e1.getMessage());
			return false;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			Log.d(LOG_TAG, "NoSuchAlgorithmException "+e.getMessage());
			return false;
		}
		
		Log.d(LOG_TAG, "check Header  "+filecsv);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(filecsv));
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
		    //Log.d(LOG_TAG, "line  "+line);
		    
			br.close(); 
			 
		    byte[] bytesOfMessage = line.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(bytesOfMessage);
			String fileMD5 = Base64.encodeToString(thedigest,Base64.DEFAULT);
		
			Log.d(LOG_TAG, "File md5 Header "+fileMD5+ "\nMaster md5 Header "+ masterMD5);
			if (fileMD5.equals(masterMD5)){
				Log.d(LOG_TAG, "CSV HEADER OK!");
				return true;
			}
			
		   
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return false;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			return false;
		}
		    
		Log.d(LOG_TAG, "CSV HEADER KO!");
		return false;
	}
	
	private static String getFileExtension(File file) {
	    String name = file.getName();
	    try {
	        return name.substring(name.lastIndexOf("."));

	    } catch (Exception e) {
	        return "";
	    }

	} 
	public static String  listAllCSV() {
		
		List<String> results = new ArrayList<String>();
		File[] files = new File("/usbdisk").listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null. 
		
			
			String[] commands_chk = {
					"touch /usbdisk/.check",
				};
				AndroidExec axc_chk = new AndroidExec();
				axc_chk.RunAsRoot(commands_chk);
				File fcheck = new File("/usbdisk/.check");
				if(fcheck.exists()){
					Log.d(LOG_TAG, "/usbdisk is NULL");
					String[] commandsdelete = {
							"rm /usbdisk/.check",
						};
						AndroidExec axc_del = new AndroidExec();
						axc_del.RunAsRoot(commandsdelete);
						
						for (File file : files) {
						    if (file.isFile()) {
						    	Log.d(LOG_TAG, "file ="+file.getName());
						    	String ext =getFileExtension(file) ;
						    	Log.d(LOG_TAG, "ext ="+ext);
						    	if (ext.equalsIgnoreCase(".CSV")
						    			&& !file.getName().startsWith("_")
						    			&& !file.getName().startsWith(".")
						    			)
						        results.add(file.getName());
						    }
						}
						Log.d(LOG_TAG, "results size="+results.size());
						if (results.size() == 0){
							
							 return "ZERO";
						}
						
						if (results.size() > 1){
							return "MANY";
							}
						
						return results.get(0);
					 
				}else{
					Log.d(LOG_TAG, "/usbdisk is NULL");
				 return "NOMOUNT";
				}
			
		
			
		
	} 
	
	
	public static void update(String filcsv){
		// TODO Auto-generated method stub
		
		
			String[] commands = {
				"cp  \""+filcsv+"\" /sdcard/db.csv",
			};
			AndroidExec axc_net = new AndroidExec();
			axc_net.RunAsRoot(commands);
		
		
	}
}
