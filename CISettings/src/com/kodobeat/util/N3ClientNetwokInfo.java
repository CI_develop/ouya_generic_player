package com.kodobeat.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.os.Build;
import android.util.Log;

public class N3ClientNetwokInfo {
	public static String LOG_TAG ="IESNetwokInfo";
	public N3ClientNetwokInfo() {
		// TODO Auto-generated constructor stub
	}

	public String[] getNetdevices()
	{

		return new String[]{"lo","eth0","wlan0","p2p0"};


	}

	public String getIPbydev( String dev) {
		
		String s = null;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {


			try {


				Process p = Runtime.getRuntime().exec("getprop net." + dev+".ip");
				BufferedReader stdInput = new BufferedReader(new
						InputStreamReader(p.getInputStream()));
				// read the output from the command
				//Log.d(LOG_TAG,"Here is the standard output of the command:\n");
				while ((s = stdInput.readLine()) != null) {
					Log.d(LOG_TAG, s);
					Log.d(LOG_TAG, "IP " + s);
					return s;
				}


			} catch (IOException e) {
				Log.d(LOG_TAG, "exception happened - here's what I know: ");
				e.printStackTrace();

			}

		}
		else {

			try {


				Process p = Runtime.getRuntime().exec("ifconfig " + dev);
				BufferedReader stdInput = new BufferedReader(new
						InputStreamReader(p.getInputStream()));
				// read the output from the command
				//Log.d(LOG_TAG,"Here is the standard output of the command:\n");
				while ((s = stdInput.readLine()) != null) {
					Log.d(LOG_TAG, s);

					String inet = search(dev + ": ip [0-9.]+", s, dev + ": ip ");
					Log.d(LOG_TAG, "IP " + inet);
					return inet;
				}


			} catch (IOException e) {
				Log.d(LOG_TAG, "exception happened - here's what I know: ");
				e.printStackTrace();

			}

		}
		return "";
	}


	private String search(String regex, String line, String removeString)
	{
		Pattern compiledPattern = Pattern.compile(regex);
		Matcher matcher = compiledPattern.matcher(line);
		String ipAddress = null;
		if (matcher.find())
		{
			ipAddress = matcher.group().replaceFirst(removeString, "");
		}
		return ipAddress;
	}
	
	
	/**
	 * Gets the local IP address by looping over the
	 * {@link NetworkInterface#getNetworkInterfaces()}.
	 * 
	 * @return the IP address of the first network interface that isn't loopback
	 *         and isn't a linkLocalAddress
	 */
	public static InetAddress getLocalIpAddress() {
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface
					.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf
						.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()
							&& !inetAddress.isLinkLocalAddress()) {
						return inetAddress;
					}
				}
			}
		} catch (SocketException ex) {
			Log.e(LOG_TAG, ex.toString());
		}
		return null;
	}
	
}
