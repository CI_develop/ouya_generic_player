package com.kodobeat.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.kodobeat.util.ReadTHINCVS.Brands;

import android.content.ClipData.Item;
import android.gesture.Prediction;
import android.util.Log;

public class ReadTHINCVS {
	
	 class City{
		private String city;
		private Thin thin;
		
		
		public City(String city, Thin thin) {
			super();
			
			this.city = city;
			this.thin = thin;
		}
		public int hashCode(){
	        return city.hashCode();
	    }
	   //
		public boolean equals(Object obj){
	       
	        if (obj instanceof City) {
	        	City pp = (City) obj;
	        	if (pp.city.equalsIgnoreCase(this.city)){
	        		
	        		return true;
	        	}
	        } else {
	            return false;
	        }
	        return false;
	    }
		
		
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public Thin getThin() {
			return thin;
		}
		public void setThin(Thin thin) {
			this.thin = thin;
		}
		
		
		
		
	}
	
	
	
	
	class Brands{
		private String brands;
		private Thin thin;
		//
		public int hashCode(){
	        return brands.hashCode();
	    }
	   //
		public boolean equals(Object obj){
	       
	        if (obj instanceof Brands) {
	        	Brands pp = (Brands) obj;
	            return (pp.brands.equals(this.brands));
	        } else {
	            return false;
	        }
	    }
		
		public String getBrands() {
			return brands;
		}
		public void setBrands(String brands) {
			this.brands = brands;
		}
		public Thin getThin() {
			return thin;
		}
		public void setThin(Thin thin) {
			this.thin = thin;
		}
		public Brands(String brands, Thin thin) {
			super();
			this.brands = brands;
			this.thin = thin;
		}
	}
	
	
	public class SortByCity implements Comparator<City>{
	    public int compare(City n1, City n2) {
	        return n1.getCity().compareTo(n2.getCity());
	    }
	}

	
	private static final String LOG_TAG = "ReadTHINCVS";
	public String line = "";
	public static String cvsSplitBy = ",";
	private ArrayList <Thin> thins ;
	public ArrayList<Thin> getThins() {
		return thins;
	}
	public ReadTHINCVS(InputStream inputStream) {
		try {
		    thins = convertInputStreamToArray(inputStream);
		  
		    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			thins = null;
		}
		
	}
	
// ritorna la lista dei Brands
public ArrayList <Thin> getBrands() {
	/*
	String brand = "";
	ArrayList <Thin> brands = new ArrayList<Thin>();
	while (iter.hasNext()) {
		Thin thin = iter.next();
		String locaBrand = thin.getBrand();
		if (!brand.equalsIgnoreCase(locaBrand)){
			brand = locaBrand;
			brands.add(thin);
		}
		
	}
	*/
	ArrayList <Thin> brands = new ArrayList<Thin>();
	
	Iterator<Thin> iter = thins.iterator();
	
	HashSet<Brands> hashset = new HashSet<Brands>();
	
	while (iter.hasNext()) {
			Thin thin = iter.next();
			
			hashset.add(new Brands(thin.getBrand(),thin));
		}
	
	
	Iterator<Brands> b = hashset.iterator();
	
	
	
	    while ( b.hasNext() ){
	    	brands.add(b.next().getThin());
	    }
	    
	    
return brands;
	
}

//ritorna la lista dei tot
public Thin gettotByID(int id) {
	Iterator<Thin> iter = thins.iterator();
	
	while (iter.hasNext()) {
		Thin thin = iter.next();
		
		if (thin.getIndex() == id){
			return thin;
		}
		
	}
return null;
	
}


// Crea una parte di Json che verra poi richiamara da gettotot byid

public String getMonitorNameByID(int id) {
	Iterator<Thin> iter = thins.iterator();
	String monitor="";
	int howmany = 0;
	while (iter.hasNext()) {
		Thin thin = iter.next();
		
		if (thin.getIndex() == id){
			if (null != thin.getHowmany())
			{
				howmany = Integer.parseInt(thin.getHowmany());
			//marco mofica
			} else 
			{
				howmany = 0;
			}
				for (int i=0;i<howmany;i++){
			
				monitor+="{\"monitor\":\""+ thin.getMonitor_name()+"\"},";
				if (iter.hasNext())
					thin = iter.next();
			}
			if (monitor.length()>0)
				monitor = monitor.substring(0,monitor.length()-1);
			return monitor;
		}
		
	}
	
	
return "";
	
}

//ritorna la lista delle citta by brands
public ArrayList <Thin> getCityByBrand(String brand) {
	Iterator<Thin> iter = thins.iterator();
	/*
	Iterator<Thin> iterfirstcity = thins.iterator();
	
	ArrayList <Thin> cities = new ArrayList<Thin>();
	int howmany = 0;
	String city =  "";
	
	
	
	while (iterfirstcity.hasNext()) {
		Thin thin = iterfirstcity.next();
		String localbrand = thin.getBrand();
		if (brand.equalsIgnoreCase(localbrand)){
			city = thin.getCitta();
			break;
		}
	}
	
	Log.d(LOG_TAG, "starting city" +city);
	Thin thinprev = null;
	while (iter.hasNext()) {
		Thin thin = iter.next();
		
		String localbrand = thin.getBrand();
			
		Log.d(LOG_TAG, "localbrand " +localbrand);
		if (brand.equalsIgnoreCase(localbrand)){
			Log.d(LOG_TAG, "localbrand == "+brand);
			String locacity = thin.getCitta();
			Log.d(LOG_TAG, "locacity is "+locacity);
			if (!city.equalsIgnoreCase(locacity)  ){
				Log.d(LOG_TAG, "locacity != "+city);
				cities.add(thinprev);
				howmany = 1;
				
			}else{ 
				Log.d(LOG_TAG, "locacity == "+city);
				howmany++; 
				Log.d(LOG_TAG, "city " +city  +" # "+howmany);
				thin.setHowmany(""+howmany);
				thinprev = thin;
			}
			city = locacity ;
			
		}else{
			if (null!= thinprev){
				cities.add(thinprev);
				thinprev = null;
			}
				
			
			
		}
		
		
	}
	*/
	ArrayList <Thin> cities = new ArrayList<Thin>();
	//HashSet<City> hashset = new HashSet<City>();
	TreeSet<City> hashset = new TreeSet<City>(new SortByCity());
	while (iter.hasNext()) {
		Thin thin = iter.next();
		if (thin.getBrand().equalsIgnoreCase(brand))
			{
				hashset.add(new City(thin.getCitta(),thin));
				
			}
	}
	// ordina le citta
	
	
	Iterator<City> b = hashset.iterator();
    while ( b.hasNext() ){
    	City c = b.next();
    	int howmany =0 ;
    	//Log.d(LOG_TAG, "CITTA"+c.getCity() );
    	howmany = countCity(c.getCity());
    	c.getThin().setHowmany(""+howmany);
    	cities.add(c.getThin());
    }

return cities;
	
}

private int countCity(String city) {
	Iterator<Thin> iter = thins.iterator();
	int hmany = 0;
	while (iter.hasNext()) {
		Thin thin = iter.next();
		if (thin.getCitta().equalsIgnoreCase(city))
			{
			hmany++;
				
			}
	}
	Log.d(LOG_TAG, "city " +city +"# "+hmany);
	return hmany;
}
//ritorna la lista delle citta by brands
public ArrayList <Thin> getNetworkbyID(int id) {
	Iterator<Thin> iter = thins.iterator();
	
	ArrayList <Thin> thinnet = new ArrayList<Thin>();
	while (iter.hasNext()) {
		Thin thin = iter.next();
		int localID = thin.getIndex();
		if (localID == id){
			thinnet.add(thin);
			}
			
		}
return thinnet;
	
}

// convert inputstream to Array
private static ArrayList <Thin> convertInputStreamToArray(InputStream inputStream) throws IOException{
			BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
			String line = "";
			ArrayList <Thin> thins = new ArrayList<Thin>();
			int index = 1;
			//remove Heaers
			line = bufferedReader.readLine();
			while((line = bufferedReader.readLine()) != null){
				
				if (line.length()>0){
					
				Thin thin = new Thin();
				String[] fields = line.split(";");
				
				Log.d(LOG_TAG, "fields #" +fields.length);
				
				
				thin.setBrand(fields[0]);
				thin.setCodice(fields[1]);
				thin.setHostname(fields[2]);;
				thin.setCitta("["+fields[1]+"] "+fields[3]);
				thin.setIp(fields[4]);
				thin.setGateway(fields[5]);
				thin.setSottorete(fields[6]);
				thin.setDns1(fields[7]);
				thin.setDns2(fields[8]);
				thin.setInstoreip(fields[9]);
				thin.setInstoreip_backup(fields[10]);
				if (fields.length == 12){
					thin.setMonitor_name(fields[11]);
				}else
					{
					thin.setMonitor_name("");
					}
				
				thin.setIndex(index);
				index++;
				thins.add(thin);
				
				}
				
				
				
			}
			inputStream.close();
			return thins;

		}
	
}
