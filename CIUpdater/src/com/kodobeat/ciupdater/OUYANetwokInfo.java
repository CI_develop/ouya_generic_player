package com.kodobeat.ciupdater;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;

public class OUYANetwokInfo {
	public static String LOG_TAG ="OUYANetwokInfo";
	public OUYANetwokInfo() {
		// TODO Auto-generated constructor stub
	}

	public String[] getNetdevices()
	{

		return new String[]{"lo","eth0","wlan0","p2p0"};


	}

	public String getIPbydev( String dev) {
		String s = null;
		try {
			Process p = Runtime.getRuntime().exec("ifconfig "+dev);
			BufferedReader stdInput = new BufferedReader(new
					InputStreamReader(p.getInputStream()));
			// read the output from the command
			while ((s = stdInput.readLine()) != null) {
				Log.d(LOG_TAG,s);
				String inet = search(dev+": ip [0-9.]+", s, dev+": ip ");
				Log.d(LOG_TAG,"IP "+inet);
				return inet;
			}
		}
		catch (IOException e) {
			Log.d(LOG_TAG,"exception happened - here's what I know: ");
			e.printStackTrace();
		}
		return "";
	}

	public String getnetMaskbydev( String dev) {
		String s = null;
		try {
			Process p = Runtime.getRuntime().exec("ifconfig "+dev);
			BufferedReader stdInput = new BufferedReader(new
					InputStreamReader(p.getInputStream()));
			// read the output from the command
			while ((s = stdInput.readLine()) != null) {
				Log.d(LOG_TAG,s);
				String inet = search(" mask [0-9.]+", s," mask ");
				Log.d(LOG_TAG," mask  "+inet);
				return inet;
			}
		}
		catch (IOException e) {
			Log.d(LOG_TAG,"exception happened - here's what I know: ");
			e.printStackTrace();
		}
		return "";
	}

	private String search(String regex, String line, String removeString)
	{
		Pattern compiledPattern = Pattern.compile(regex);
		Matcher matcher = compiledPattern.matcher(line);
		String ipAddress = null;
		if (matcher.find())
		{
			ipAddress = matcher.group().replaceFirst(removeString, "");
		}
		return ipAddress;
	}
	
	
	/**
	 * Gets the local IP address by looping over the
	 * {@link NetworkInterface#getNetworkInterfaces()}.
	 * 
	 * @return the IP address of the first network interface that isn't loopback
	 *         and isn't a linkLocalAddress
	 */
	public static InetAddress getLocalIpAddress() {
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface
					.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf
						.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()
							&& !inetAddress.isLinkLocalAddress()) {
						return inetAddress;
					}
				}
			}
		} catch (SocketException ex) {
			Log.e(LOG_TAG, ex.toString());
		}
		return null;
	}
	
}
