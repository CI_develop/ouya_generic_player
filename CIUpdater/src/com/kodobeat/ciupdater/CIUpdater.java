package com.kodobeat.ciupdater;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;











import org.apache.commons.io.FilenameUtils;
import org.apache.http.protocol.HTTP;

import com.kodobeat.ciupdater.NanoHTTPD.Response.Status;

import android.R.bool;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;

public class CIUpdater extends NanoHTTPD {
	private static final String VERSIONE = "1.0.0.1";
	private static Context context;
	private ActivityManager am;
	private static Activity activity;
	//private Properties prop;
	
	private static String LOG_TAG ="CIUpdater";
	private static String nameOfViewerPkg = "com.kodobeat.civiewer";
	private static String nameOfViewerClass = "com.kodobeat.civiewer.Viewer";
	private static String nameOfSettingPkg = "com.kodobeat.cisettings";
	private static String nameOfSettingClass = "com.kodobeat.cisettings.SettingsActivity";
	
	private static String wwwadminpath ="/admin/";

	private static String fileproperties = "/sdcard/config.properties";
	private Properties prop;
	private AssetManager assetManager;
	
	
	
	
	

	public CIUpdater(Activity activity,String ip) {
		super(ip,8181);
		// TODO Auto-generated constructor stub
		CIUpdater.activity = activity;
		CIUpdater.context = activity.getApplicationContext();
				am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		assetManager = activity.getAssets();
		prop = new Properties();
		InputStream propIn = null;
		Log.d(LOG_TAG,"fileproperties :"+fileproperties);
		try {
			propIn = new FileInputStream(fileproperties);
			prop.load(propIn);
		}catch (Exception e)
		{
			e.printStackTrace();
		}finally {
			Utils.closeInputStream(propIn);
		}


	}

	


	@Override 
	public Response serve(IHTTPSession session) {
		super.serve(session);
		Method method = session.getMethod();
		String uri = session.getUri();
		Map<String, String> parms = session.getParms();
		Log.d(LOG_TAG,"method:"+method + " uri:'" + uri + "' ");
		Log.d(LOG_TAG,"parms:"+parms );
		Log.d(LOG_TAG, "Version:" +VERSIONE);
		
	/*	if (uri.equalsIgnoreCase("/getversions")){
			
			return new NanoHTTPD.Response(Status.OK,MIME_HTML,getPackages());
		}
	^/
		/*
		 * Firmware Upload
		 * */
		
		if (uri.contains(wwwadminpath)  ){
			// FROM ASSEST 
			InputStream fis = null;
			//FileInputStream fis = null;
			Response res = null;
			try {
				// Opening file from SD Card
				String filename = FilenameUtils.getName(uri);
				Log.w(LOG_TAG, "filename by get :"+filename);
				// FROM ASSEST 
				fis = assetManager.open(filename);
				//File root = Environment.getExternalStorageDirectory();
				//fis = new FileInputStream(root.getAbsolutePath() +fileadminpath+filename);

				if (filename.contains(".png")){
					res = new NanoHTTPD.Response(Status.OK,MIME_PNG,fis);
				}
				if (filename.contains(".gif")){
					res = new NanoHTTPD.Response(Status.OK,MIME_GIF,fis);
				}

				if (filename.contains(".css"))
					res  =  new NanoHTTPD.Response(Status.OK,MIME_CSS,fis);
				if (filename.contains(".js"))
					res =  new NanoHTTPD.Response(Status.OK,MIME_JS,fis);
				if (filename.contains(".html"))
					res =  new NanoHTTPD.Response(Status.OK,MIME_HTML,fis);

				return res;
			}
			catch(Exception ioe) {
				Log.e(LOG_TAG," serve files"+ ioe.toString());
				return new NanoHTTPD.Response(Status.NOT_FOUND,MIME_HTML,"");
			}finally {
				//closeInputStream(fis);
			}

		}

		
		if (uri.equalsIgnoreCase("/N3/remoteControl/checkUpdates")){
			// Froza Updater check
			//String checkupURL = "http://cloud.contentinterface.it:8080/N3/TEST/api/thinClientUpdater";
			String checkupURL = "http://"+prop.getProperty("activeUrl")+prop.getProperty("baseurl")+"/api/thinClientUpdater" ;
			
			new CheckUpdater().execute(checkupURL); 
			return new NanoHTTPD.Response("Success");
		}
		
		if (uri.equalsIgnoreCase("/firmware")){
			
			if (null!= parms.get("apk")){
				String apk = parms.get("apk");
			Log.d(LOG_TAG,"apk updated "+apk);
			if (apk.equalsIgnoreCase("CISettings.apk"))
			{
				stopSettings();
				String[] commands = {
						"pm uninstall "+nameOfSettingPkg ,
						"mount -o remount,rw /system /system",
						"cp /sdcard/"+apk+" /system/app ",
						"chmod 644 /system/app/"+apk,
						"rm /data/app/"+nameOfSettingPkg+"*.apk",
						"pm install -r -d /system/app/"+apk,
						"am start -n "+nameOfSettingPkg+"/"+nameOfSettingClass};
				Log.d(LOG_TAG,"comandi:"+commands.toString());
				AndroidExec axc = new AndroidExec();
				axc.RunAsRoot(commands);
				
			}
			if (apk.equalsIgnoreCase("CIViewer.apk"))
			{
				stopViewer();
				String[] commands = {
						"pm uninstall "+nameOfViewerPkg,
						//"pm install -r -d /sdcard/"+apk,
						"mount -o remount,rw /system /system",
						"cp /sdcard/"+apk+" /system/app ",
						"chmod 644 /system/app/"+apk,
						"rm /data/app/"+nameOfViewerPkg+"*.apk",
						"pm install -r -d /system/app/"+apk,
						"am start -n "+nameOfViewerPkg+"/"+nameOfViewerClass};
				Log.d(LOG_TAG,"comandi:"+commands.toString());
				AndroidExec axc = new AndroidExec();
				axc.RunAsRoot(commands);
			
			}
			
				return new NanoHTTPD.Response("Success");
			
			}
		}
		
	
		Log.d(LOG_TAG,"Redirect to Firmware Home");
		Response res  = new NanoHTTPD.Response(Status.REDIRECT,MIME_HTML,"");
		res.addHeader( "Location", "/admin/index.html" );
		return res;

	}


	





	


	// Ferma l'esecuzione del Viewer

	public void stopViewer(){
		if (isPackageRunning(nameOfViewerPkg)){
			AndroidExec ax = new AndroidExec();
			String[] commands = {"pkill -9 "+nameOfViewerPkg};
			ax.RunAsRoot(commands);
		}
	}
		public void stopSettings(){
			if (isPackageRunning(nameOfSettingPkg)){
				AndroidExec ax = new AndroidExec();
				String[] commands = {"pkill -9 "+nameOfSettingPkg};
				ax.RunAsRoot(commands);
			}
			
		
	}

	


	public boolean isPackageRunning(String packagename) {
		return findPIDbyPackageName(packagename) != -1;
	}
	public int findPIDbyPackageName(String packagename) {
		int result = -1;

		if (am != null) {
			for (RunningAppProcessInfo pi : am.getRunningAppProcesses()){
				//Log.d("Http","pi.processName "+pi.processName);
				if (pi.processName.equalsIgnoreCase(packagename)) {
					//result = pi.pid;
					//android.os.Process.sendSignal(pi.pid, android.os.Process.SIGNAL_QUIT); 
					Log.d("Http","pi.processName "+pi.processName+" "+pi.pid);
					//android.os.Process.killProcess(pi.pid);
					return pi.pid;
				}
				if (result != -1) break;
			}
		} else {
			result = -1;
		}

		return result;
	}



	

	/* Checks if external storage is available to at least read */
	public boolean isExternalStorageReadable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state) ||
				Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return true;
		}
		return false;
	}


	
	
	
	 class PInfo {
		    private String appname = "";
		    private String pname = "";
		    private String versionName = "";
		    private int versionCode = 0;
		
		    public String getAppname() {
				return appname;
			}

			public void setAppname(String appname) {
				this.appname = appname;
			}

			public String getPname() {
				return pname;
			}

			public void setPname(String pname) {
				this.pname = pname;
			}

			public String getVersionName() {
				return versionName;
			}

			public void setVersionName(String versionName) {
				this.versionName = versionName;
			}

			public int getVersionCode() {
				return versionCode;
			}

			public void setVersionCode(int versionCode) {
				this.versionCode = versionCode;
			}

			
		}

	/*	private String getPackages() {
		    ArrayList<PInfo> apps = getInstalledApps(false); 
		    final int max = apps.size();
		    String jsonresp = "[";
		    for (int i=0; i<max; i++) {
		        Log.d(LOG_TAG, jsonresp);
		        if (apps.get(i).getPname().contains("kodobeat")){
		        	jsonresp+="{\"appname\":\""+apps.get(i).getAppname()+"\","
		        			+ "\"pname\":\""+apps.get(i).getPname()+"\","
		        			+ "\"versionName\":\""+apps.get(i).getVersionName()+"\"},";
		        }
		    }
		    jsonresp = jsonresp.substring(0, jsonresp.length()-1);
		    jsonresp+="]";
		    return jsonresp;
		}

		private ArrayList<PInfo> getInstalledApps(boolean getSysPackages) {
		    ArrayList<PInfo> res = new ArrayList<PInfo>();        
		    List<PackageInfo> packs = activity.getPackageManager().getInstalledPackages(0);
		    for(int i=0;i<packs.size();i++) {
		        PackageInfo p = packs.get(i);
		        if ((!getSysPackages) && (p.versionName == null)) {
		            continue ;
		        }
		        PInfo newInfo = new PInfo();
		        newInfo.appname = p.applicationInfo.loadLabel(activity.getPackageManager()).toString();
		        newInfo.pname = p.packageName;
		        newInfo.versionName = p.versionName+"."+p.versionCode;
		        newInfo.versionCode = p.versionCode;
		       
		        res.add(newInfo);
		    }
		    return res; 
		}
	 
*/
	

	
}
