package com.kodobeat.ciupdater;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

public class StartServer extends AsyncTask <Activity, Void,Void> {
	private static final String LOG_TAG = "StartServer";
	private static String fileproperties = "/sdcard/config.properties";
	
	@Override
	protected Void doInBackground(Activity... params) {
		OUYANetwokInfo info = new OUYANetwokInfo();
		InputStream propIn = null;
	    String ip =  "";
		
		try {
			
			Properties prop = new Properties();
			
			Log.d(LOG_TAG,"fileproperties :"+fileproperties);
			propIn = new FileInputStream(fileproperties);
			prop.load(propIn);
			ip = info.getIPbydev(prop.getProperty("netinterface","eth0"));	
			
			
			Log.d("UpdaterActivity", "Starting...on "+prop.getProperty("netinterface")+" ip ="+ip);

			//attendo che venga attiva la rete
			while (ip.isEmpty())
			{
				ip = info.getIPbydev(prop.getProperty("netinterface","eth0"));	
				try {
					Log.d("UpdaterActivity", "waiting for. "+prop.getProperty("netinterface")+" ip = "+ip);
					Thread.sleep(6000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			Utils.closeInputStream(propIn);
			
			}
		
		
		
		
		

		 CIUpdater server = new CIUpdater(params[0],ip);
		try {
			server.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	

}
