package com.kodobeat.ciupdater;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

import android.util.Log;

public class DownloadandInstall {
	private static final String LOG_TAG = "DownloadandInstall";
	private static String nameOfViewerPkg = "com.kodobeat.civiewer";
	private static String nameOfViewerClass = "com.kodobeat.civiewer.Viewer";
	private static String nameOfSettingPkg = "com.kodobeat.cisettings";
	private static String nameOfSettingClass = "com.kodobeat.cisettings.SettingsActivity";
	private Properties prop;
	private static String fileproperties = "/sdcard/config.properties";
	
	public DownloadandInstall(String url,String apk, String versione) throws Exception {
		prop = new Properties();
		InputStream propIn = null;
		try {
			propIn = new FileInputStream(fileproperties);
			prop.load(propIn);


			if (apk.equalsIgnoreCase("CIViewer.apk"))
			{
				downloadApk(url,apk);
				stopViewer();
				String[] commands = {
						"pm uninstall "+nameOfViewerPkg,
						//"pm install -r -d /sdcard/"+apk,
						"mount -o remount,rw /system /system",
						"cp /sdcard/"+apk+" /system/app ",
						"chmod 644 /system/app/"+apk,
						"rm /data/app/"+nameOfViewerPkg+"*.apk",
						"pm install -r -d /system/app/"+apk};
				Log.d(LOG_TAG,"comandi:"+commands.toString());
				AndroidExec axc = new AndroidExec();
				axc.RunAsRoot(commands);
				prop.setProperty("viewerversion", versione);

			}

			if (apk.equalsIgnoreCase("CISettings.apk"))
			{
				downloadApk(url,apk);
				stopSettings();
				String[] commands = {
						"pm uninstall "+nameOfSettingPkg ,
						"mount -o remount,rw /system /system",
						"cp /sdcard/"+apk+" /system/app ",
						"chmod 644 /system/app/"+apk,
						"rm /data/app/"+nameOfSettingPkg+"*.apk",
						"pm install -r -d /system/app/"+apk};
				Log.d(LOG_TAG,"comandi:"+commands.toString());
				AndroidExec axc = new AndroidExec();
				axc.RunAsRoot(commands);
				
				prop.setProperty("settingversion", versione);
				

			}
			saveProperties();

		} catch (Exception e) {
			
			throw new Exception();
		}finally{
			Utils.closeInputStream(propIn);
		}

		

	}


	
	
	
	public void stopViewer(){

		AndroidExec ax = new AndroidExec();
		String[] commands = {"pkill -9 "+nameOfViewerPkg};
		ax.RunAsRoot(commands);

	}
	
	public void stopSettings(){

		AndroidExec ax = new AndroidExec();
		String[] commands = {"pkill -9 "+nameOfSettingPkg};
		ax.RunAsRoot(commands);

	}
	
	private  synchronized void saveProperties() throws IOException
	{
		
			OutputStream output;
			output = new FileOutputStream(fileproperties);
			prop.store(output, null);
			output.flush();
			output.close();
			// se alcune propriet richiedono reboot

	}

	protected void downloadApk(String sUrl, String fileapk) throws Exception {
		InputStream input = null;
		OutputStream output = null;
		HttpURLConnection connection = null;
		try {
			URL url = new URL(sUrl);
			connection = (HttpURLConnection) url.openConnection();
			connection.addRequestProperty("User-Agent", "ThinClient");
			connection.setConnectTimeout(2000);
			connection.setReadTimeout(2000);
			connection.connect();

			// expect HTTP 200 OK, so we don't mistakenly save error report
			// instead of the file
			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new Exception();
				
			}

			// this will be useful to display download percentage
			// might be -1: server did not report the length
			int fileLength = connection.getContentLength();

			// download the file
			input = connection.getInputStream();
			output = new FileOutputStream("/sdcard/"+fileapk);

			byte data[] = new byte[4096];
			long total = 0;
			int count;
			while ((count = input.read(data)) != -1) {
				// allow canceling with back button

				total += count;

				output.write(data, 0, count);
			}
		} catch (Exception e) {
			throw new Exception();
		} finally {
			try {
				if (output != null)
					output.close();
				if (input != null)
					input.close();
			} catch (IOException ignored) {
				throw new Exception();
			}

			if (connection != null)
				connection.disconnect();
		}
		
	}


}
