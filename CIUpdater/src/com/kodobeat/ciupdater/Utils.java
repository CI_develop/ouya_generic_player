package com.kodobeat.ciupdater;

import java.io.IOException;
import java.io.InputStream;

public class Utils {

	public static void closeInputStream(InputStream is) {
		try {
			if (is != null) {
				is.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
