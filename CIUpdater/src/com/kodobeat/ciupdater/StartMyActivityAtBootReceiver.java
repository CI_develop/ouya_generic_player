package com.kodobeat.ciupdater;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.Timer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

public class StartMyActivityAtBootReceiver extends BroadcastReceiver {    
   

	private static final String LOG_TAG = "StartMyActivityAtBootReceiver";
		@Override
	public void onReceive(Context context, Intent intent) {
		
	    Log.d(LOG_TAG, "ACTION "+intent.getAction());
	 /*   String packageName = intent.getDataString ();
	    Log.d(LOG_TAG,"uninstall:" + packageName + "package name of the program");*/
		Intent i = new Intent(context, UpdaterActivity.class);  
	    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    context.startActivity(i);  
		
	    //Al boot lancio la verifica delle app. dopo 30 sec
	    Timer timer =  new Timer();
	    timer.schedule(new BootReminder(), 30 * 1000);
	}   
	
	
}