package com.kodobeat.ciupdater;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.TimerTask;

import android.util.Log;

public class BootReminder extends TimerTask {
	private static final String LOG_TAG = "Updater BootReminder";
	private static String fileproperties = "/sdcard/config.properties";

	@Override
	public void run() {
		
		Properties prop = new Properties();
		InputStream propIn = null;
		try {
			propIn = new FileInputStream(fileproperties);
			prop.load(propIn);
			String checkupURL = "http://"+prop.getProperty("activeUrl")+prop.getProperty("baseurl")+"/api/thinClientUpdater" ;
			Log.d(LOG_TAG, "Checking updates on "+checkupURL);
			new CheckUpdater().execute(checkupURL); 
		}catch (Exception e)
		{
			e.printStackTrace();
		}finally {
			Utils.closeInputStream(propIn);
		}
	}

	

}
