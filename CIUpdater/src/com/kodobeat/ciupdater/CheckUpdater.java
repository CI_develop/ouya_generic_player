package com.kodobeat.ciupdater;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint("NewApi")
public class CheckUpdater extends AsyncTask <String, Void,Void>{
	private static final String LOG_TAG = "CheckUpdater";
	private static String fileproperties = "/sdcard/config.properties";
	/*
	 * 
{
  "data": 
  [
    {
      "name": "CISettings",
      "url": "http://172.16.10.2:9090/N3/MW/api/content/4?language=ita",
      "version": "0"
    },
    {
      "name": "CIViewer",
      "url": "http://172.16.10.2:9090/N3/MW/api/content/4?language=ita",
      "version": "0"
    }
  ]
}
	 */

	


	private JSONObject obj;
	public void esegui(String jsonurl) {

		boolean needreboot = false;
		InputStream propIn = null;
		try {
			
			JSONObject json;
			Properties prop = new Properties();
			
			Log.d(LOG_TAG,"fileproperties :"+fileproperties);
			propIn = new FileInputStream(fileproperties);
			prop.load(propIn);
			json = readJsonFromUrl(jsonurl);
			//System.out.println(json.toString());
			System.out.println(json.getJSONArray("data"));
			JSONArray jsonMainArr = json.getJSONArray("data");
			for (int i = 0; i < jsonMainArr.length(); i++) {  // **line 2**
				JSONObject childJSONObject = jsonMainArr.getJSONObject(i);
				String name = childJSONObject.getString("name");
				String version    = childJSONObject.getString("version");
				String urldownload = childJSONObject.getString("url");
				Log.i(LOG_TAG, "name "+name+" version "+version+" urldownload "+urldownload);
				String actualversion ="0";
				// devo leggere dal prop la versione del sett e del viewer
				if (name.equalsIgnoreCase("CISettings"))
					actualversion  = prop.getProperty("settingversion","0");//0=default se la prima volta non lo trova
				if (name.equalsIgnoreCase("CIViewer"))
					actualversion  = prop.getProperty("viewerversion","0");
				
				if (!version.equalsIgnoreCase(actualversion)){
					//devo aggiornare
					try{
						new DownloadandInstall(urldownload, name+".apk",version);
						needreboot = true; // se almeno uno dei due si aggiorna reboot
					}
					catch(Exception e) {
						continue;
					}
				}

			}

			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			Utils.closeInputStream(propIn);
			if (needreboot){
				String[] commands = {"reboot"};
				Log.d(LOG_TAG,"comandi:"+commands);
				AndroidExec axc = new AndroidExec();
				axc.RunAsRoot(commands); 
			}
			
		}

	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		URL jsonUrl = new URL(url);
	
		HttpURLConnection conn = (HttpURLConnection) jsonUrl.openConnection();
	    conn.addRequestProperty("User-Agent", "ThinClient");
	    conn.setConnectTimeout(2000);
	    conn.setReadTimeout(2000);
	    conn.connect();
	    InputStream is = conn.getInputStream();
	
		
		//InputStream is = jsonUrl.openStream();
		
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			return json;
		} finally {
			is.close();
		}
	}

	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {
			esegui(params[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	
}
