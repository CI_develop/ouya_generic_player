package com.kodobeat.ciupdater;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;











import com.kodobeat.ciupdater.R;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.os.Build;

public class UpdaterActivity extends Activity {
	
	private static String fileproperties = "/sdcard/config.properties";
	/*private static String nameOfProcess = "com.kodobeat.civiewer";
	private static String nameOfClass = "com.kodobeat.civiewer.Viewer";
	private Properties prop;
	*/
	//private static String fileproperties = "/sdcard/config.properties";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		moveTaskToBack(true);
		setContentView(R.layout.activity_updater);
		ImageView img=  (ImageView)  findViewById(R.id.imageView1);
		
		Properties prop = new Properties();
		InputStream propIn = null;
		try {
			propIn = new FileInputStream(fileproperties);
			prop.load(propIn); 
					} catch (Exception e) {
			
					
		}finally{
			Utils.closeInputStream(propIn);
		}
		
		
		String mode = prop.getProperty("mode", "landscape");
		if (!mode.equalsIgnoreCase("landscape")){
			img.setImageResource(R.drawable.mw_portrait);
		}else{
			img.setImageResource(R.drawable.mw_landscape);
			
		}
		
		
		new StartServer().execute(this);
		
			
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.updater, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_updater,
					container, false);
			return rootView;
		}
	}

}
