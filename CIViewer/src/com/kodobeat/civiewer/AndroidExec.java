package com.kodobeat.civiewer;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import android.util.Log;

public class AndroidExec {

	public AndroidExec() {
		// TODO Auto-generated constructor stub
	}
	public boolean RunAsRoot(String[] cmds){
        Process p;
		try {
			String s = null;
			p = Runtime.getRuntime().exec("su");
			
			
			  DataOutputStream os = new DataOutputStream(p.getOutputStream());            
		        for (String tmpCmd : cmds) {
		                os.writeBytes(tmpCmd+"\n");
		                Log.d("AndroidExec", tmpCmd);
		                
		        }           
		        os.writeBytes("exit\n");  
		        os.flush();
		        os.close();
		        p.waitFor();
		       
		        Log.d("AndroidExec", "p.exitValue()=" +p.exitValue());
	          //  if (p.exitValue() == 0) return true;
	          //  else return false;
		        return true;
		        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("AndroidExec", e.getMessage());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			Log.e("AndroidExec", e.getMessage());
		}
		return false;
      
}
}
