package com.kodobeat.civiewer;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;












import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kodobeat.util.UrlCache;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class Viewer extends Activity {
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private String url;
	private String urlerror;
	private String mode;
	private String dsMode="1";
	private String action;
	private String brand;
	private static String LOG_TAG ="Viewer";
	private WebView webView;
	private boolean ferma= false;
	private PendingIntent netchkp;
	// Variable Cache
	private UrlCache urlCache;

	public class TouchandHold extends Thread {

	    public void run(){
	    	long tStart = System.currentTimeMillis();
			double elapsedSeconds = 0;
			while (elapsedSeconds < 15 && ferma)
			{
				long tEnd = System.currentTimeMillis();
				Long tDelta = tEnd - tStart;
				elapsedSeconds = tDelta / 1000.0;
				Log.d(LOG_TAG, "**onTouch:DOWN elapsedSeconds"+elapsedSeconds);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}

			}

				//Reset
			if (elapsedSeconds > 15){

				AndroidExec ax = new AndroidExec();
				String[] commands = {"rm /sdcard/local.prop","echo \"ro.sf.hwrotation=0\\nro.sf.lcd_density=160\" >/sdcard/local.prop","cp /sdcard/local.prop /data/local.prop ","rm /sdcard/config.properties","reboot"};
				Log.d(LOG_TAG,"comandi:"+commands.toString());
				AndroidExec axc = new AndroidExec();
				axc.RunAsRoot(commands);
			}
	    }
	  }
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (true) {
			moveTaskToBack(true);
			return;
		}
		 else {
					// Creazione Istanza di Cache
					this.urlCache = new UrlCache(this);

					String[] commands_int = {"service call activity 42 s16 com.android.systemui"};
					Log.d(LOG_TAG, "comandi:" + commands_int.toString());
					AndroidExec axc = new AndroidExec();
					axc.RunAsRoot(commands_int);


					View decorView = getWindow().getDecorView();

					int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
					decorView.setSystemUiVisibility(uiOptions);
					getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
					getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
					getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
					setContentView(R.layout.activity_viewer);

					Properties prop = new Properties();
					FileInputStream input = null;

					Intent intent = getIntent();
					urlerror = intent.getStringExtra("url");
					if (null == urlerror) {
						urlerror = "#";
					}


					String fileproperties = Environment.getExternalStorageDirectory().getAbsolutePath() + "/config.properties";
					Log.d(LOG_TAG, "fileproperties: " + fileproperties);

					try {
						input = new FileInputStream(fileproperties);

						prop.load(input);
						input.close();
						// set the properties value

						mode = prop.getProperty("mode");
						brand = prop.getProperty("brand");
						dsMode = prop.getProperty("dsMode", "1");

						Log.d(LOG_TAG, "urlerror: " + urlerror);
						url = prop.getProperty("url");

						/*
						if (urlerror.equalsIgnoreCase("#")){
							url = prop.getProperty("url");
							Log.d(LOG_TAG,"rendering page "+url);

						}
						else{
							//BRAND POTREBBE NON ESSERE IN ASSET
							boolean exists = assetExists(getAssets(),brand+"_"+mode+".html" );
							if (exists)
								url = "file:///android_asset/"+brand+"_"+mode+".html";
							else
								url = "file:///android_asset/default_"+mode+".html";

							}
						*/


						Log.d(LOG_TAG, "prop: " + prop);

						// save properties to project root folder
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						Log.d(LOG_TAG, e.getMessage());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						Log.d(LOG_TAG, e.getMessage());
					}


					// S Modifiche Inforce 6309
					if (null != prop && null != prop.getProperty("mode") && prop.getProperty("mode").equalsIgnoreCase("landscape")) {
						Log.d(LOG_TAG, "SCREEN_ORIENTATION_LANDSCAPE");
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

					} else {
						Log.d(LOG_TAG, "SCREEN_ORIENTATION_PORTRAIT");
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
					}


					// E Modifiche Inforce 6309


					// Impostazioni WebView

					webView = (WebView) findViewById(R.id.webView1);
					final WebSettings settings = webView.getSettings();

					settings.setJavaScriptEnabled(true);
					settings.setJavaScriptCanOpenWindowsAutomatically(true);
					settings.setPluginState(WebSettings.PluginState.ON);
					settings.setDomStorageEnabled(true);
					settings.setLoadsImagesAutomatically(true);
					settings.setSupportMultipleWindows(false);
					settings.setUseWideViewPort(true);
					settings.setLoadWithOverviewMode(false);
					settings.setSupportZoom(false);
					settings.setMediaPlaybackRequiresUserGesture(false);


					settings.setDomStorageEnabled(true);
					// Set cache size to 50 mb by default. should be more than enough
					int cachesizemb = 50;
					if (null != prop.getProperty("cache-browser"))
						cachesizemb = Integer.parseInt(prop.getProperty("cache-browser"));
					settings.setAppCachePath("/data/data/" + getPackageName() + "/cache");
					settings.setAppCacheMaxSize(1024 * 1024 * cachesizemb);
					settings.setAllowFileAccess(true);
					settings.setAppCacheEnabled(true);
					settings.setAllowUniversalAccessFromFileURLs(true);
					settings.setAllowFileAccessFromFileURLs(true);
					//WebSettings.LOAD_NO_CACHE	Don't use the cache, load from the network.
					settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

					webView.setWebViewClient(new WebViewClient() {
						/*
						@Override
						public WebResourceResponse shouldInterceptRequest(WebView view, String weburl)
						{

						 Log.d(LOG_TAG, "Check Cache url =" + weburl);
							// Esclusione dal cache dei json eWebSocket
							//if (  weburl.contains("mp4") || weburl.contains("jpg") || weburl.contains("png") )
							//if ( !weburl.contains("html") ) return null ;
							//if ( !weburl.contains("json") ) return null ;
							if ( !weburl.contains("Atmosphere") ) return null ;


							Log.d(LOG_TAG, "Cache fired ");
							urlCache.register(weburl, weburl.substring(weburl.lastIndexOf("/") + 1), "", "UTF-8", 5 * UrlCache.ONE_DAY);
							Log.d(LOG_TAG, "Cache base  =" +weburl.substring(weburl.lastIndexOf("/") + 1));


							WebResourceResponse responseW = urlCache.load(weburl);
							if (responseW!=null)
								Log.d(LOG_TAG, "Cache WebResourceResponse "+responseW.getMimeType());
							return responseW;



						}

						//private Bitmap 		mDefaultVideoPoster;
						// autoplay when finished loading via javascript injection
						@Override
						public void onPageFinished(WebView view, String url) {
							super.onPageFinished(view, url);
							//webView.loadUrl("javascript:(function() { document.getElementsByTagName('video')[0].play();document.getElementsByTagName('video')[0].loop =true;})()");
							//webView.loadUrl("javascript:function callback1() {var video=document.getElementsByTagName('video')[0];video.load(); video.play();alert('c');}");
							//webView.loadUrl("javascript:function callback1() {alert('c');}");
							//webView.loadUrl("javascript:(function() { document.getElementsByTagName('video')[0].on('ended', callback1(), false)})()");

						}

						@Override
						public void onReceivedError(WebView view, int errorCod,String description, String failingUrl) {
							Log.d(LOG_TAG,"onReceivedError "+description +" failingUrl " +failingUrl +"mode "+mode+ "barnd "+brand);

							if (null!=brand && assetExists(getAssets(),brand+"_"+mode+".html")){
								webView.loadUrl("file:///android_asset/"+brand+"_"+mode+".html");
								Log.d(LOG_TAG, "webView.loadUrl file:///android_asset/"+brand+"_"+mode+".html");

							}
							else {
								webView.loadUrl("file:///android_asset/default_" + mode + ".html");
								Log.d(LOG_TAG, "webView.loadUrl file:///android_asset/default_" + mode + ".html");
							}

						}
					*/


					});
					webView.setClickable(true);
					webView.setLongClickable(false);
					webView.setFocusableInTouchMode(true);
					webView.setWebChromeClient(new WebChromeClient() {
						// CACHE MANAGER

						@Override
						public Bitmap getDefaultVideoPoster() {
							Log.d(LOG_TAG, "here in on getDefaultVideoPoster");

							try {
								return BitmapFactory.decodeStream(getAssets().open("black.png"));
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							return null;
						}


						public boolean onConsoleMessage(ConsoleMessage cm) {
							Log.d(LOG_TAG, "console:" + cm.message());
							return true;
						}

					});


					webView.setOnTouchListener(new OnTouchListener() {


						@Override

						public boolean onTouch(View v, MotionEvent event) {

							int action = event.getAction();

							if (action == MotionEvent.ACTION_DOWN) {
								ferma = true;

								new TouchandHold().start();

							}
							if (action == MotionEvent.ACTION_UP) {
								Log.d(LOG_TAG, "**onTouch:UP");
								ferma = false;

							}
							return false;
						}
					});


					if (null != url) webView.loadUrl(url);

	}



	}


	@Override
	protected void onStart(){
		super.onStart();
		Log.d(LOG_TAG,"onStart:");

		 Log.i(LOG_TAG, "Starting LM Checker");

		    Intent netchk = new Intent(this, LMAlarmCheck.class);
			netchk.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			netchkp = PendingIntent.getBroadcast(this, 0, netchk, PendingIntent.FLAG_UPDATE_CURRENT);
			AlarmManager amnetchk = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			amnetchk.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 120*1000, netchkp);



	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		Log.d(LOG_TAG,"onPostCreate:");

	}




	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(LOG_TAG,"onDestroy:");

		AlarmManager amnetchk = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
         amnetchk.cancel(netchkp);
	}



	/* Checks if external storage is available to at least read */
	public boolean isExternalStorageReadable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state) ||
				Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return true;
		}
		return false;
	}

	private static boolean assetExists(AssetManager assets, String name) {
	    try {
	        // using File to extract path / filename
	        // alternatively use name.lastIndexOf("/") to extract the path
	        File f = new File(name);
	        String parent = f.getParent();
	        if (parent == null) parent = "";
	        String fileName = f.getName();
	        // now use path to list all files
	        String[] assetList = assets.list(parent);
	        if (assetList != null && assetList.length > 0) {
	            for (String item : assetList) {
	                if (fileName.equals(item))
	                    return true;
	            }
	        }
	    } catch (IOException e) {
	        // Log.w(TAG, e); // enable to log errors
	    }
	    return false;
	}

}
