package com.kodobeat.civiewer;




import com.kodobeat.util.Util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class LMAlarmCheck extends BroadcastReceiver {

	private static final String LOG_TAG = "LMAlarmCheck";
	
	private static String nameOfSettingPkg = "com.kodobeat.cisettings";
	private static String nameOfViewerPkg = "com.kodobeat.civiewer";
	private static String nameOfUpdaterPkg = "com.kodobeat.ciupdater";
	
	
	private static String nameOfUpdaterClass = "com.kodobeat.ciupdater.UpdaterActivity";
	private static String nameOfSettingClass = "com.kodobeat.cisettings.SettingsActivity";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		boolean viewerstart = false;
		
		Log.d(LOG_TAG, "Alarm fired");
		if (!Util.isPackageRunning(nameOfUpdaterPkg,context)){
			Log.d(LOG_TAG, "Updater NOT Running");
			
			String[] commands = {
			"am start -n "+nameOfUpdaterPkg+"/"+nameOfUpdaterClass};
			Log.d(LOG_TAG,"comandi:"+commands.toString());
			AndroidExec axc = new AndroidExec();
			axc.RunAsRoot(commands);
			Log.d(LOG_TAG, "Started Updater");
			
		}
		if (!Util.isPackageRunning(nameOfSettingPkg,context)){
			Log.d(LOG_TAG, "Settings NOT Running");
			
			Intent in = new Intent(Intent.ACTION_MAIN);
			in.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
			in.addCategory(Intent.CATEGORY_LAUNCHER);
			in.setClassName(nameOfSettingPkg, nameOfSettingClass); 
			context.startActivity(in); 
			
		    Log.d(LOG_TAG, "Started Settings");
		    
			Log.d(LOG_TAG, "kill  mediaserver maybe is big");
				AndroidExec ax = new AndroidExec();
				String[] commands = {"pkill -9 "+nameOfViewerPkg,
						"killall -9  mediaserver",};
				ax.RunAsRoot(commands);
			

		    
		}
		
		
	
	}


		

}
